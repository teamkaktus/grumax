<?php
/**
 *
 */
class ControllerModuleContact extends Controller
{
    public function index()
    {
      $this->document->addScript('\admin\view\javascript\contact.js');
     //$this->document->addScript('\admin\view\javascript\google-places.js');
     $this->document->addScript('http://maps.googleapis.com/maps/api/js?libraries=places&v=3.23');
      $this->document->addScript('\admin\view\javascript\jquery.geocomplete.js');
      // C:\wamp\www\grumax\admin\view\javascript\contact.js
      $this->document->setTitle('Страница Контакты');
      $this->load->model('setting/setting');
      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
        $this->session->data['success'] = "Успешно";
        // var_dump($this->request->post);``
        $pre_contact_data = $this->request->post;
        $contact_data = [];
        if (!empty($pre_contact_data)) {
          foreach ($pre_contact_data['title'] as $key => $value) {
            $contact_data[] = array(
              'title' => $pre_contact_data['title'][$key],
              'description' => $pre_contact_data['data'][$key],
              'xpos' => $pre_contact_data['xpos'][$key],
              'ypos' => $pre_contact_data['ypos'][$key],
              'adress_db' => $pre_contact_data['adress_db'][$key],
              'images' => (isset($pre_contact_data['image'][$key]))? $pre_contact_data['image'][$key]:[],
            );
          }
        }
      // var_dump($contact_data);exit;
        $contact_data = json_encode($contact_data);
        $this->model_setting_setting->editSetting('contact', ['contact_data'=> $contact_data]);
        $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
      }
      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
        'text' => $this->language->get('text_home'),
        'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
      );

      $data['breadcrumbs'][] = array(
        'text' => "Модули",
        'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
      );

      $data['breadcrumbs'][] = array(
        'text' => 'Контакты',
        'href' => $this->url->link('module/contact', 'token=' . $this->session->data['token'], 'SSL')
      );
      $data['header'] = $this->load->controller('common/header');
      $data['column_left'] = $this->load->controller('common/column_left');
      $data['footer'] = $this->load->controller('common/footer');

      $data['action'] = $this->url->link('module/contact', 'token=' . $this->session->data['token'], 'SSL');

      $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

      $aData = $this->model_setting_setting->getSetting('contact');
      if (isset($aData)) {
        $data['aData'] = (isset($aData['contact_data'])) ? $aData['contact_data'] : '{}' ;
      }else {
          $data['aData'] = '{}';
      }
      // $data['aData'] = $aData['contact_data'];

      $this->response->setOutput($this->load->view('module/contact.tpl', $data));

    }
    protected function validate() {
      return true;
    }
    public function install()
    {
      $this->load->model('setting/setting');
      $this->model_setting_setting->editSetting('contact',["contact_status" => 1]);
    }
    public function uninstall()
    {
      $this->load->model('setting/setting');
      $this->model_setting_setting->editSetting('contact',["contact_status" => 0]);
    }

}

 ?>
