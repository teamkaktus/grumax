<?php
// Heading
$_['heading_title']       				   = '<span style="color:#0000ff;">Dependent Options</span> <span style="height:20px;width:117px;display:inline-block;margin-top:8px;background:no-repeat 50% 50%;"></span><span style="display:inline-block;margin-top:8px;float:right;font-weight:bold;">Download Feofan.net</span>
';

// Text
$_['text_no_config']					   = 'There are no configurations here required for this extension. The extension has been installed successfully.';
$_['text_heading']		  				   = 'Dependent Options';
$_['text_module']         				   = 'Modules';
$_['text_edit']							   = 'Edit Module Dependent Options';
$_['text_purchase_import']				   = 'Purchase Dependent Options Import / Export';

// Tab
$_['tab_general']		  				   = 'General';
$_['tab_import']		  				   = 'Import / Export';

// Error
$_['error_permission']    				   = 'Warning: You do not have permission to modify Dependent Options!';