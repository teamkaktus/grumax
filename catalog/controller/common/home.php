<?php

class ControllerCommonHome extends Controller
{
    public function index()
    {
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        $this->load->model('catalog/action');

        $action_product_id = $this->model_catalog_action->getActionProductsHome();

        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        foreach ($action_product_id as $product_id) {
            $product_info = $this->model_catalog_product->getProduct($product_id['product_id']);


            if ($product_info['image']) {
                $image = $this->model_tool_image->resize($product_info['image'], 80, 80);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 80, 80);
            }

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $price = false;
            }

            if ((float)$product_info['special']) {
                $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $special = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
            } else {
                $tax = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = (int)$product_info['rating'];
            } else {
                $rating = false;
            }

            $data['products'][] = array(
                'product_id'  => $product_info['product_id'],
                'thumb'       => $image,
                'name'        => $product_info['name'],
                'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                'attribute_groups' => $this->model_catalog_product->getProductAttributes($product_info['product_id']),
                'stock'       => $product_info['quantity'],
                'price'       => $price,
                'special'     => $special,
                'tax'         => $tax,
                'minimum'     => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
                'rating'      => $rating,
                'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
            );
        }

        if (isset($this->request->get['route'])) {
            $this->document->addLink(HTTP_SERVER, 'canonical');
        }

        $this->load->model('catalog/manufacturer');
        $product_infosManufactures = $this->model_catalog_manufacturer->getManufacturers();

        $data['manufactures_url'] = $this->url->link('product/manufacturer');

        $data['manufactures'] = $product_infosManufactures;
        $data['all_special'] = $this->url->link('information/action');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
        }
    }
}