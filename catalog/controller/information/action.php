<?php

class ControllerInformationAction extends Controller
{
    public function index()
    {
        $this->load->language('information/action');

        $this->load->model('catalog/action');


        $actions_info = $this->model_catalog_action->getActions();

        $this->document->setTitle($this->language->get('title'));
        $this->document->setDescription('');
        $this->document->setKeywords('');

        $data['heading_title'] = $this->language->get('title');
        $this->load->model('tool/image');

        $data['actions'] = array();
        foreach ($actions_info as $action) {
            $data['actions'][] = array(
                'title' => $action['title'],
                'short_description' => html_entity_decode($action['short_description'], ENT_QUOTES, 'UTF-8'),
                'img' => $this->model_tool_image->resize($action['image'], 234, 135),
                'href' => $this->url->link('information/action/detail', 'action_id=' . $action['action_id'])
            );
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/action_list.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/action_list.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/information/action_list.tpl', $data));
        }
    }

    public function detail()
    {
        $this->load->language('information/action');

        $this->load->model('catalog/action');

        if (isset($this->request->get['action_id'])) {
            $action_id = (int)$this->request->get['action_id'];
        } else {
            $action_id = 0;
        }

        $action_info = $this->model_catalog_action->getAction($action_id);

        if ($action_info) {

            $this->document->setTitle($action_info['meta_title']);
            $this->document->setDescription($action_info['meta_description']);
            $this->document->setKeywords($action_info['meta_keyword']);

            $data['heading_title'] = $action_info['title'];

            $data['description'] = html_entity_decode($action_info['description'], ENT_QUOTES, 'UTF-8');

            $data['img'] = $action_info['image'];

            $action_products = $this->model_catalog_action->getActionProducts($action_id);
            $data['action_products'] = array();

            $data['continue'] = $this->url->link('information/action');

            $this->load->model('catalog/product');
            $this->load->model('tool/image');

            foreach ($action_products as $action_product) {

                $product = $this->model_catalog_product->getProduct($action_product['product_id']);

                if ($product['image']) {
                    $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$product['special']) {
                    $special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product['special'] ? $product['special'] : $product['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int)$product['rating'];
                } else {
                    $rating = false;
                }

                $data['action_products'][] = array(
                    'product_id' => $product['product_id'],
                    'thumb' => $image,
                    'name' => $product['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'attribute_groups' => $this->model_catalog_product->getProductAttributes($product['product_id']),
                    'stock' => $product['quantity'],
                    'price' => $price,
                    'special' => $special,
                    'tax' => $tax,
                    'minimum' => $product['minimum'] > 0 ? $product['minimum'] : 1,
                    'rating' => $product['rating'],
                    'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
                );
            }

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/action.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/action.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/information/action.tpl', $data));
            }


        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('information/information', 'information_id=' . $information_id)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
            }
        }
    }
}