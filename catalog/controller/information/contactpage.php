<?php
class ControllerInformationContactpage extends Controller {
    public function index() {
      $this->document->addScript('\catalog\view\javascript\jquery.prettyPhoto.js');
      $this->document->addScript('http://maps.googleapis.com/maps/api/js?libraries=places&v=3.23');
        $this->document->addScript('\catalog\view\javascript\contact.js');
        $this->document->addStyle('\catalog\view\theme\default\stylesheet\prettyPhoto.css');
        $this->document->setTitle($this->config->get('config_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $data['heading_title'] = $this->config->get('config_title');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $this->load->model('setting/setting');
        $contact_data = $this->model_setting_setting->getSetting('contact');
        if (isset($contact_data)) {
          $data['jData'] = (isset($contact_data['contact_data'])) ? $contact_data['contact_data'] : '{}' ;
        }else {
            $data['jData'] = '{}';
        }
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/contactpage.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/contactpage.tpl', $data));
        } else {
                $this->response->setOutput($this->load->view('default/template/information/contactpage.tpl', $data));
        }
    }
}
?>
