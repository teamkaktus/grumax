<?php
// Heading
$_['heading_title']            = 'Личный Кабинет';

// Text
$_['text_account']             = 'Личный Кабинет';
$_['text_my_profile']          = 'Профиль';
$_['text_my_purchase_history'] = 'История покупок';
$_['text_my_wish_list']        = 'Список желаний';
$_['text_my_comparison_list']  = 'Список сравнения';
$_['text_my_cart']             = 'Корзина';



// **************** profile ****************


// Text
$_['pr_text_your_details']  = 'Личные данные';
$_['pr_text_success']       = 'Ваши личные данные успешно обновлены!';
$_['button_change']       = 'Изменить';

// Entry
$_['pr_entry_firstname']    = 'Имя';
$_['pr_entry_lastname']     = 'Фамилия';
$_['pr_entry_email']        = 'Эл. почта';
$_['pr_entry_telephone']    = 'Телефон';

// Error
$_['pr_error_exists']       = 'Данный E-Mail уже зарегистрирован!';
$_['pr_error_firstname']    = 'Имя должно быть от 1 до 32 символов!';
$_['pr_error_lastname']     = 'Фамилия должна быть от 1 до 32 символов!';
$_['pr_error_email']        = 'E-Mail адрес введен неверно!';
$_['pr_error_telephone']    = 'Номер телефона должен быть от 3 до 32 символов!';
$_['pr_error_custom_field'] = '%s необходим!';
// **************** /profile ***************

// **************** password ***************

// Text
$_['ps_text_your_password']  = 'Пароль';
$_['ps_text_success']   = 'Ваш пароль успешно изменен!';

// Entry
$_['ps_entry_password'] = 'Пароль';
$_['ps_entry_confirm']  = 'Подтвердите пароль';

// Error
$_['ps_error_password'] = 'Пароль должен быть от 4 до 20 символов!';
$_['ps_error_confirm']  = 'Пароли и пароль подтверждения не совпадают!';

// **************** /password **************

// **************** ADDRESS ****************
// Text
$_['as_text_address_book']    = 'Список адресов доставки';
$_['as_text_edit_address']    = 'Редактировать адрес';
$_['as_text_add']             = 'Ваш адрес был успешно добавлен';
$_['as_text_edit']            = 'Ваш адрес был успешно изменен';
$_['as_text_delete']          = 'Ваш адрес был успешно удален';
$_['as_text_empty']           = 'Список адресов в аккаунте, пуст.';
$_['as_button_addAddress']    = 'Добавить';

// Entry
$_['as_entry_firstname']      = 'Имя, Отчество';
$_['as_entry_lastname']       = 'Фамилия';
$_['as_entry_company']        = 'Компания';
$_['as_entry_address_1']      = 'Адрес 1';
$_['as_entry_address_2']      = 'Адрес 2';
$_['as_entry_postcode']       = 'Почтовый индекс';
$_['as_entry_city']           = 'Город';
$_['as_entry_country']        = 'Страна';
$_['as_entry_zone']           = 'Регион / Область';
$_['as_entry_default']        = 'Основной адрес';

// Error
$_['as_error_delete']         = 'Вы должны добавить не менее 1 адреса!';
$_['as_error_default']        = 'Вы не можете удалить основной адрес!';
$_['as_error_firstname']      = 'Имя должно быть от 1 до 32 символов!';
$_['as_error_lastname']       = 'Фамилия должна быть от 1 до 32 символов!';
$_['as_error_vat']            = 'Неверный индекс!';
$_['as_error_address_1']      = 'Адрес должен быть от 3 до 128 символов!';
$_['as_error_postcode']       = 'Индекс должен быть от 2 до 10 символов!';
$_['as_error_city']           = 'Название города должно быть от 2 до 128 символов!';
$_['as_error_country']        = 'Пожалуйста, укажите страну!';
$_['as_error_zone']           = 'Пожалуйста, укажите регион / область!';
$_['as_error_custom_field']   = 'Необходимо заполнить "%s"!';

// **************** /ADDRESS ***************

// **************** wishlist ***************
$_['wl_text_instock']  = 'В наличии';
$_['wl_text_wishlist'] = 'Закладки (%s)';
$_['wl_text_login']    = 'Вы должны <a href="%s">выполнить вход</a> или <a href="%s">создать аккаунт</a> чтобы сохранить <a href="%s">%s</a> в свой <a href="%s">список закладок</a>!';
$_['wl_text_success']  = 'Вы добавили <a href="%s">%s</a> в <a href="%s">Закладки</a>!';
$_['wl_text_exists']   = '<a href="%s">%s</a> уже есть <a href="%s">закладках</a>!';
$_['wl_text_remove']   = 'Список закладок успешно изменен!';
$_['wl_text_empty']    = 'Ваш список желаний пуст';

// Column
$_['wl_column_image']  = 'Изображение';
$_['wl_column_name']   = 'Название товара';
$_['wl_column_model']  = 'Модель';
$_['wl_column_stock']  = 'Наличие';
$_['wl_column_price']  = 'Цена за единицу товара';
$_['wl_column_action'] = 'Действие';

// **************** /wishlist **************

// **************** order ***************

// Text
$_['ph_text_order_detail']     = 'Детали заказа';
$_['ph_text_invoice_no']       = '№ Счета';
$_['ph_text_order_id']         = '№ Заказа';
$_['ph_text_date_added']       = 'Добавлено';
$_['ph_text_shipping_address'] = 'Адрес доставки';
$_['ph_text_shipping_method']  = 'Способ доставки';
$_['ph_text_payment_address']  = 'Платёжный адрес';
$_['ph_text_payment_method']   = 'Способ оплаты';
$_['ph_text_comment']          = 'Комментарий к заказу';
$_['ph_text_history']          = 'История заказов';
$_['ph_text_success']          = 'Товары из заказа <a href="%s">%s</a> успешно добавлены <a href="%s">в вашу корзину</a>!';
$_['ph_text_empty']            = 'Вы еще не совершали покупок!';
$_['ph_text_error']            = 'Запрошенный заказ не найден!';

// Column
$_['ph_column_order_id']       = '№ Заказа';
$_['ph_column_product']        = '№ Товара';
$_['ph_column_customer']       = 'Клиент';
$_['ph_column_name']           = 'Название товара';
$_['ph_column_model']          = 'Модель';
$_['ph_column_quantity']       = 'Количество';
$_['ph_column_price']          = 'Цена';
$_['ph_column_total']          = 'Всего';
$_['ph_column_action']         = 'Действие';
$_['ph_column_date_added']     = 'Добавлено';
$_['ph_column_status']         = 'Статус';
$_['ph_column_comment']        = 'Комментарий';

// Error
$_['ph_error_reorder']         = '%s в данный момент не доступен....';

// **************** /order **************

// **************** compare *************
$_['cm_text_compare']     = 'Сравнение товаров';

// Text
$_['cm_text_product']      = 'Описание';
$_['cm_text_name']         = 'Товар';
$_['cm_text_image']        = 'Изображение';
$_['cm_text_price']        = 'Цена';
$_['cm_text_model']        = 'Модель';
$_['cm_text_manufacturer'] = 'Производитель';
$_['cm_text_availability'] = 'Наличие';
$_['cm_text_instock']      = 'Есть на складе';
$_['cm_text_rating']       = 'Рейтинг';
$_['cm_text_reviews']      = 'Всего отзывов: %s';
$_['cm_text_summary']      = 'Краткое описание';
$_['cm_text_weight']       = 'Вес';
$_['cm_text_dimension']    = 'Размеры (Д х Ш х В)';
$_['cm_text_compare']      = 'Сравнение товаров (%s)';
$_['cm_text_success']      = 'Товар <a href="%s">%s</a> добавлен в ваш <a href="%s">список сравнения</a>!';
$_['cm_text_remove']       = 'Удалено из списка сравнений';
$_['cm_text_empty']        = 'Вы не выбрали ни одного товара для сравнения.';

// **************** /compare ************