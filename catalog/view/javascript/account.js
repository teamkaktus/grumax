$(window).load(function () {

    $('.sub_menu_sravnit').click(function () {
        var id_tabs = $(this).data('id');

        setTimeout(initial, 1000);

        function initial() {
            //$('#'+id_tabs).find('.owl-carousel').css('border','1px solid red');
            $('#' + id_tabs).find('.owl-carousel').owlCarousel({
                loop: true,
                margin: 5,
                nav: true,
                responsive: {
                    0: {
                        items: 2
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 5
                    }
                }
            });
        }

    });

    $('.comparison_list_tabs').click(function () {
        var id_tabs = $(".sub_menu_sravnit").first().data('id');
        //alert(id_tabs);
        setTimeout(initial, 1000);

        function initial() {
            //$('#'+id_tabs).find('.owl-carousel').css('border','1px solid red');
            $('#' + id_tabs).find('.owl-carousel').owlCarousel({
                loop: true,
                margin: 5,
                nav: true,
                responsive: {
                    0: {
                        items: 2
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 5
                    }
                }
            });
        }
    });

    $('.input_image_color').change();

    $('.form-group[data-sort]').detach().each(function () {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
            $('.form-group').eq($(this).attr('data-sort')).before(this);
        }

        if ($(this).attr('data-sort') > $('.form-group').length) {
            $('.form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('.form-group').length) {
            $('.form-group:first').before(this);
        }
    });

    $('.pr_form-group[data-sort]').detach().each(function () {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.pr_form-group').length) {
            $('.pr_form-group').eq($(this).attr('data-sort')).before(this);
        }

        if ($(this).attr('data-sort') > $('.pr_form-group').length) {
            $('.pr_form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('.pr_form-group').length) {
            $('.pr_form-group:first').before(this);
        }
    });

    $('.as_form-group[data-sort]').detach().each(function () {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.as_form-group').length) {
            $('.as_form-group').eq($(this).attr('data-sort')).before(this);
        }

        if ($(this).attr('data-sort') > $('.as_form-group').length) {
            $('.as_form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('.as_form-group').length) {
            $('.as_form-group:first').before(this);
        }
    });

    $('button[id^=\'button-custom-field\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $(node).parent().find('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });

    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });

    var hash_array = ['#profile', '#purchase_history', '#wish_list', '#comparison_list'],
        account_hash = document.location.hash;
    if (account_hash !== '' && $.inArray(account_hash, hash_array) !== -1) {
        $(".navbar-nav > li").each(function (index, elem) {
            if ($(elem).children('a').attr('href') == account_hash) {
                $(".navbar-nav > li").removeClass('active');
                $(elem).addClass('active');
                return false;
            }
        });

        $('.tab-pane').removeClass('active');
        $(account_hash).addClass('active');

    }


    $('.form-group[data-sort]').detach().each(function () {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
            $('.form-group').eq($(this).attr('data-sort')).before(this);
        }

        if ($(this).attr('data-sort') > $('.form-group').length) {
            $('.form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('.form-group').length) {
            $('.form-group:first').before(this);
        }
    });


    $('button[id^=\'button-custom-field\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $(node).parent().find('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });

    $('select[name=\'country_id\']').on('change', function () {
        $.ajax({
            url: 'index.php?route=account/account/country&country_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function () {
                $('.fa-spin').remove();
            },
            success: function (json) {
                if (json['postcode_required'] == '1') {
                    $('input[name=\'postcode\']').parent().parent().addClass('required');
                } else {
                    $('input[name=\'postcode\']').parent().parent().removeClass('required');
                }

                html = '<option value=""><?php echo $text_select; ?></option>';

                if (json['zone'] && json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                        if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                }

                $('select[name=\'zone_id\']').html(html);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'country_id\']').trigger('change');

    $("#wish_list .inc-q").click(function () {
        edit_quantity('+', $(this));
    });
    $("#wish_list .dec-q").click(function () {
        edit_quantity('-', $(this));
    });

    $('#categ_tab a:first').tab('show');

});

function edit_quantity(prefix, element_change) {
    switch (prefix) {
        case '-':
        {
            var quantity = parseInt(element_change.next('#wl-quantity').val());
            if (quantity > 1) {
                quantity -= 1;
            }
            element_change.next('#wl-quantity').val(quantity);
            break;
        }
        case '+':
        {
            var quantity = parseInt(element_change.prev('#wl-quantity').val());
            quantity += 1;
            element_change.prev('#wl-quantity').val(quantity);
            break;
        }
    }
}

var ph_cart = {
    'add': function (product_id) {

        //alert('#product_'+ product_id +' input[type=\'text\'], #product_'+ product_id +' input[type=\'hidden\'], #product_'+ product_id +' input[type=\'radio\']:checked, #product_'+ product_id +' input[type=\'checkbox\']:checked, #product_'+ product_id +' select, #product textarea');
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#ph-product_' + product_id + ' input[type=\'text\'], #ph-product_' + product_id + ' input[type=\'hidden\'], #ph-product_' + product_id + ' input[type=\'radio\']:checked, #ph-product_' + product_id + ' input[type=\'checkbox\']:checked, #ph-product_' + product_id + ' select, #ph-product_' + product_id + ' textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#ph-button-cart').button('Добавление');
            },
            complete: function () {
                $('#ph-button-cart').button('Готово');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.ph-form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('.ph-wrap-success').append('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#cart > a').html('<span id="cart-total"><i class="fa fa-shopping-cart" style="color:#f9a51e;" ></i> ' + json['total'] + '</span>');

                    $('html, body').animate({scrollTop: 0}, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
};

var wl_cart = {
    'add': function (product_id) {

        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#wl-product_' + product_id + ' input[type=\'text\'], #wl-product_' + product_id + ' input[type=\'number\'], #wl-product_' + product_id + ' input[type=\'hidden\'], #wl-product_' + product_id + ' input[type=\'radio\']:checked, #wl-product_' + product_id + ' input[type=\'checkbox\']:checked, #wl-product_' + product_id + ' select, #wl-product_' + product_id + ' textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#wl-button-cart').button('Добавление');
            },
            complete: function () {
                $('#wl-button-cart').button('Готово');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.ph-form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('.wl-wrap-success').append('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#cart > a').html('<span id="cart-total"><i class="fa fa-shopping-cart" style="color:#f9a51e;" ></i> ' + json['total'] + '</span>');

                    $('html, body').animate({scrollTop: 0}, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
};

$(function(){
    $('select[name^=\'option\']').trigger('change');
});

$(document).on('change', 'select[name^=\'option\']', function () {
    $(this).after('<i class="fa fa-spinner selection-wait"></i>');

    var value = $(this).val();
    var parent_id = $(this).attr('data-parent');

    var poduct_id = $(this).attr('data-product_id');

    $.ajax({
        url: 'index.php?route=product/product/dependentoption&parent_id=' + parent_id + '&value=' + value + '&product_id=' + poduct_id,
        type: 'get',
        dataType: 'json',
        success: function (json) {
            $('.selection-wait').remove();

            if (json['option']) {
                for (i = 0; i < json['option'].length; i++) {
                    if (json['option'][i]['type'] == 'select') {
                        $('#input-option' + json['option'][i]['product_option_id']).stop().fadeOut('medium');

                        $('#input-option' + json['option'][i]['product_option_id']).siblings('.control-label').stop().fadeOut('medium');

                        var html = '';

                        for (j = 0; j < json['option'][i]['option_value'].length; j++) {
                            $('#input-option' + json['option'][i]['product_option_id']).fadeIn('medium');

                            $('#input-option' + json['option'][i]['product_option_id']).siblings('.control-label').fadeIn('medium');

                            html += '<option value="' + json['option'][i]['option_value'][j]['product_option_value_id'] + '">' + json['option'][i]['option_value'][j]['name'];

                            if (json['option'][i]['option_value'][j]['price']) {
                                html += ' (' + json['option'][i]['option_value'][j]['price_prefix'] + json['option'][i]['option_value'][j]['price'] + ')';
                            }

                            html += '</option>';
                        }

                        $('select[name=\'option[' + json['option'][i]['product_option_id'] + ']\']').html(html);
                    } else if (json['option'][i]['type'] == 'radio' || json['option'][i]['type'] == 'checkbox' || json['option'][i]['type'] == 'image') {
                        $('#input-option' + json['option'][i]['product_option_id']).stop().fadeOut('medium');

                        $('#input-option' + json['option'][i]['product_option_id']).siblings('.control-label').stop().fadeOut('medium');

                        $('#input-option' + json['option'][i]['product_option_id']).children().hide();

                        $('#input-option' + json['option'][i]['product_option_id']).find('input').prop('checked', false);

                        for (j = 0; j < json['option'][i]['option_value'].length; j++) {
                            $('#input-option' + json['option'][i]['product_option_id']).fadeIn('medium');

                            $('#input-option' + json['option'][i]['product_option_id']).siblings('.control-label').fadeIn('medium');
                            $('#input-option' + json['option'][i]['product_option_id']).children().fadeIn('medium');

                            $('#input-option' + json['option'][i]['product_option_id']).find('input[value=\'' + json['option'][i]['option_value'][j]['product_option_value_id'] + '\']').parent().show();

                            $('#input-option' + json['option'][i]['product_option_id']).find('input[value=\'' + json['option'][i]['option_value'][j]['product_option_value_id'] + '\']').parent().parent().show();
                        }
                    } else {
                        // File, text, textarea, date, datetime
                        if (json['option'][i]['option_value']) {
                            $('#input-option' + json['option'][i]['product_option_id']).parents('.wl-form-group').stop().fadeIn('medium');
                        } else {
                            $('#input-option' + json['option'][i]['product_option_id']).parents('.wl-form-group').stop().fadeOut('medium');
                        }
                    }
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

