$(function () {

//    $('.inputColor').change(function(){
//        $(this).parent().parent().parent().find('img').removeClass('img-thumbnail').addClass('img-responsive');
//        $(this).parent().find('img').removeClass('img-responsive').addClass('img-thumbnail');
//    })

    $('select[name^=\'option\']').trigger('change');

    jQuery(".input-quantity").keypress(
        function (event) {
            var key, keyChar;
            if (!event) var event = window.event;
            if (event.keyCode) key = event.keyCode;
            else if (event.which) key = event.which;

            if (key == 13) {
                edit_cart_option($(this).attr('data-cart_id'));
            }

            if (key == null || (key >= 48 && key <= 57)) return true;
            keyChar = String.fromCharCode(key);
            if (!/\d/.test(keyChar)) return false;
        });
    jQuery(".input-quantity").change(
        function (event) {
            var tmp = parseInt($(this).val(), 10) ? parseInt($(this).val(), 10) : 1;
            if (!Number($(this).val())) {
                $(this).val(tmp);
            }
            edit_cart_option($(this).attr('data-cart_id'));
            event.preventDefault();
        });

    $(".dec-c-quantity").click(function () {
        edit_cart_quantity('-', $(this).parent().parent());
    });
    $(".inc-c-quantity").click(function () {
        edit_cart_quantity('+', $(this).parent().parent());
    });

    $(".cart-form-group input").change(function (event) {
        edit_cart_option($(this).attr('data-cart_id'));
        event.preventDefault();
    });

    $(".cart-form-group select").change(function (event) {
        edit_cart_option($(this).attr('data-cart_id'));
        event.preventDefault();
    });


    $("#all_add_wishlist").click(function () {
        var wishlist = '';
        $(".cart_product").each(function (index, elem) {
            if (index == 0) {
                wishlist += 'wishlist[' + index + ']=' + $(elem).attr('data-product_id');
            } else {
                wishlist += '&wishlist[' + index + ']=' + $(elem).attr('data-product_id');
            }
        });

        $.ajax({
            url: 'index.php?route=account/wishlist/add_list',
            type: 'post',
            data: wishlist,
            dataType: 'json',
            beforeSend: function () {

            },
            complete: function () {
                $("#wishlist-success-text").html('<div class="alert alert-success"><i class="fa fa-check-circle"></i id="wishlist-text-success"> Все товары успешно добавлены в список желаний!<button type="button" class="close" data-dismiss="alert">×</button></div>');
            },
            success: function (json) {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                //alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    /*$(document).on('click', '.productColor', function (event) {
        var prod_img = $(this).attr('data-product-image');
        var popup_prod_img = $(this).attr('data-popup-product-image');
        if ($(this).data('parentoption_id') == '14') {
            var dataOptionid = $(this).data('option_id');
            changeColorPogonazh(dataOptionid);
        }
        if (prod_img != '') {
            $("#thumb_img").attr('src', prod_img);
            $("#popup_thumb_img").attr('href', popup_prod_img);
        }
        console.log('checkColor');
    });*/

    $(".cart_product ").each(function(index, elem){
        $(elem).find('.cartColor').on('click', function(event){
            var prod_img = $(this).attr('data-product-image');
            $(elem).find("img.cart-image").attr('src', prod_img);
        })
    });
});

$(document).on('change', 'select[name^=\'option\']', function () {
    $(this).after('<i class="fa fa-spinner selection-wait"></i>');

    var value = $(this).val();
    var parent_id = $(this).attr('data-parent');
    var cart_id = $(this).attr('data-cart_id');

    var poduct_id = $(this).attr('data-product_id');

    var select_obj = $(this);

    $.ajax({
        url: 'index.php?route=product/product/dependentoption&parent_id=' + parent_id + '&value=' + value + '&product_id=' + poduct_id,
        type: 'get',
        dataType: 'json',
        success: function (json) {
            $('.selection-wait').remove();
            if(select_obj.children('option:selected').attr('data-hint') != ''){
                select_obj.next().html(select_obj.children('option:selected').attr('data-hint'));

            }
            if (json['option']) {
                console.log(json['option']);
                for (i = 0; i < json['option'].length; i++) {
                    if (json['option'][i]['type'] == 'select') {
                        $('#input-option' + json['option'][i]['product_option_id']+cart_id).stop().fadeOut('medium');
                        $('#input-option' + json['option'][i]['product_option_id']+cart_id).siblings('.control-label').stop().fadeOut('medium');

                        var html = '';

                        for (j = 0; j < json['option'][i]['option_value'].length; j++) {
                            
                            $('#input-option' + json['option'][i]['product_option_id']+cart_id).fadeIn('medium');

                            $('#input-option' + json['option'][i]['product_option_id']+cart_id).siblings('.control-label').fadeIn('medium');

                            html += '<option value="' + json['option'][i]['option_value'][j]['product_option_value_id'] + '">' + json['option'][i]['option_value'][j]['name'];

                            if (json['option'][i]['option_value'][j]['price']) {
                                html += ' (' + json['option'][i]['option_value'][j]['price_prefix'] + json['option'][i]['option_value'][j]['price'] + ')';
                            }

                            html += '</option>';
                        }

                        $('select[name=\'option[' + json['option'][i]['product_option_id']+cart_id + ']\']').html(html);
                    } else if (json['option'][i]['type'] == 'radio' || json['option'][i]['type'] == 'checkbox' || json['option'][i]['type'] == 'image') {
                        $('#input-option' + json['option'][i]['product_option_id']+cart_id).stop().fadeOut('medium');
                        console.log('#input-option' + json['option'][i]['product_option_id']+cart_id);

                        $('#input-option' + json['option'][i]['product_option_id']+cart_id).siblings('.control-label').stop().fadeOut('medium');

                        //$('#input-option' + json['option'][i]['product_option_id']+cart_id).children().hide();

                        //$('#input-option' + json['option'][i]['product_option_id']+cart_id).find('input').prop('checked', false);

                        for (j = 0; j < json['option'][i]['option_value'].length; j++) {
                            $('#input-option' + json['option'][i]['product_option_id']+cart_id).fadeIn('medium');

                            $('#input-option' + json['option'][i]['product_option_id']+cart_id).siblings('.control-label').fadeIn('medium');

                            $('#input-option' + json['option'][i]['product_option_id']+cart_id).find('input[value=\'' + json['option'][i]['option_value'][j]['product_option_value_id'] + '\']').parent().show();

                            $('#input-option' + json['option'][i]['product_option_id']+cart_id).find('input[value=\'' + json['option'][i]['option_value'][j]['product_option_value_id'] + '\']').parent().parent().show();
                        }
                    } else {
                        // File, text, textarea, date, datetime
                        if (json['option'][i]['option_value']) {
                            $('#input-option' + json['option'][i]['product_option_id']+cart_id).parents('.form-group').stop().fadeIn('medium');
                        } else {
                            $('#input-option' + json['option'][i]['product_option_id']+cart_id).parents('.form-group').stop().fadeOut('medium');
                        }
                    }
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

function edit_cart_option(id) {
    $.ajax({
        url: 'index.php?route=checkout/cart/update_cart',
        type: 'post',
        data: $('.cart_' + id + ' input[type=\'text\'], .cart_' + id + ' input[type=\'hidden\'], .cart_' + id + ' input[type=\'radio\']:checked, .cart_' + id + ' input[type=\'checkbox\']:checked, .cart_' + id + ' select, .cart_' + id + ' textarea'),
        dataType: 'json',
        beforeSend: function () {

        },
        complete: function () {

        },
        success: function (json) {
            $('.alert, .text-danger').remove();
            $('.ph-form-group').removeClass('has-error');

            if (json['error']) {
                if (json['error']['option']) {
                    for (i in json['error']['option']) {
                        var element = $('#input-option' + i.replace('_', '-'));

                        if (element.parent().hasClass('input-group')) {
                            element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                        } else {
                            element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                        }
                    }
                }

                if (json['error']['recurring']) {
                    $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                }

                // Highlight any found errors
                $('.text-danger').parent().addClass('has-error');
            }

            if (json['success']) {
                $('.ph-wrap-success').append('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                $('#cart > a').html('<span id="cart-total"><i class="fa fa-shopping-cart" style="color:#f9a51e;" ></i> ' + json['total'] + '</span>');

                //$('html, body').animate({scrollTop: 0}, 'slow');

                $(".product_" + id + "_price").html(json['product_total_price']);
                $("#cart_total_price").html(json['cart_total']);

                $('#cart > ul').load('index.php?route=common/cart/info ul li');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}

function edit_cart_quantity(prefix, obj) {
    var quantity = parseInt(obj.children("input[type='text']").val()),
        id = obj.children("input[type='text']").attr('data-cart_id');
    switch (prefix) {
        case '-':
        {
            if (quantity > 1) {
                quantity -= 1;
            }
            break;
        }
        case '+':
        {
            quantity += 1;
            break;
        }
    }
    obj.children("input[type='text']").val(quantity);
    edit_cart_option(id);
}