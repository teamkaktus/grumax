$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            if ($('.selected_fields').html() != '') {
                $('.yourSelectFilter').show();
            }
        } else {
            $('.yourSelectFilter').hide();
        }
    });

    $(document).on('change', '.floor_coverings .coverage', function(){
        $('.floor_coverings .countComplect').text($(this).val());
    });

    $('select[name^="option"]').trigger('change');

    function edit_cart_quantityC(prefix, obj, button) {
        var quantity = parseInt(obj.children("input[type='text']").val()),
            id = obj.children("input[type='text']").attr('data-cart_id'),
            product = (button.attr('data-product')) ? button.attr('data-product') : 1,
            minQuantity = $("#product_list_" + product + " .blockComplectArrow input[name='minQuantity']").val();
        if (!minQuantity) {
            minQuantity = 1;
        }
        console.log(button.attr('data-product'));
        switch (prefix) {
            case '-':
            {
                if (quantity > minQuantity) {
                    quantity -= 1;
                }

                changeNameC(quantity, button);
                break;
            }
            case '+':
            {
                quantity += 1;

                changeNameC(quantity, button);
                break;
            }
        }
        obj.children("input[type='text']").val(quantity);
    }

    function changeNameC(quantity, button) {
        if (button.attr('data-product')) {
            var p_id = button.attr('data-product');
            var price_obj = $("#product_list_" + p_id + " .blockComplectArrow .price .price-new");
            var price = '';
            if($("#product_list_" + p_id + " .blockWithComplectC input[name='priceArea']").length > 0 && $("#product_list_" + p_id + " .blockWithComplectC input[name='priceArea']").val() != ''){
                price = parseFloat($("#product_list_" + p_id + " .blockWithComplectC input[name='priceArea']").val());
            }else {
                price = parseFloat($("#product_list_" + p_id + " .blockWithComplectC input[name='old_price']").val());
            }
            var new_price = price * (quantity);
            price_obj.html(new_price + "₽");
        }
    }

    $(document).on('change', "input[name='quantity']", function () {
        changeNameC($(this).val(), $(this))
    });

    $(document).on('click', '.dec-c-quantityC', function () {
        edit_cart_quantityC('-', $(this).parent().parent(), $(this));
    });
    $(document).on('click', '.inc-c-quantityC', function () {
        edit_cart_quantityC('+', $(this).parent().parent(), $(this));
    });

    $(".partition").on('change', '#width_value', function(event){
        var p_id = $(this).attr('data-product');
        if(!isNaN(parseInt($(this).val())) &&  !isNaN(parseInt($(".partition #height_value").val()))){
            var area = (parseInt($(this).val()) / 1000) * (parseInt($(".partition #height_value").val()) / 1000);
            $("#product_list_" + p_id + " .partition #area_value").html(area.toFixed(2) + 'м<sup>2</sup>');
            $("#product_list_" + p_id + " .partition input[name='area_value']").val(area.toFixed(2));
            changePriceArea(area.toFixed(2), p_id);
        }else{
            $("#product_list_" + p_id + " .partition #area_value").html('1м<sup>2</sup>');
            $("#product_list_" + p_id + " .partition input[name='area_value']").val(1);
            changePriceArea(area.toFixed(2), p_id);
        }
        event.preventDefault();
    });

    $(".partition").on('change', '#height_value', function(event){
        var p_id = $(this).attr('data-product');
        if(!isNaN(parseInt($(this).val())) &&  !isNaN(parseInt($(".partition #width_value").val()))){
            var area = (parseInt($(this).val()) / 1000) * (parseInt($(".partition #width_value").val()) / 1000);
            $("#product_list_" + p_id + " .partition #area_value").html(area.toFixed(2) + 'м<sup>2</sup>');
            $("#product_list_" + p_id + " .partition input[name='area_value']").val(area.toFixed(2));
            changePriceArea(area.toFixed(2), p_id)
        }else{
            $("#product_list_" + p_id + " .partition #area_value").html('1м<sup>2</sup>');
            $("#product_list_" + p_id + " .partition input[name='area_value']").val(1);
            changePriceArea(area.toFixed(2), p_id)
        }
        event.preventDefault();
    });
});

function changePriceArea(area, p_id){
    var quantity = 1;
    var oldPrice = $("#product_list_" + p_id + " .blockWithComplectC input[name='old_price']").val();
    var oldComplectName = $("#product_list_" + p_id + " .blockWithComplectC input[name='ComplectName']").val();
    if($("#product_list_" + p_id + " .blockWithComplectC input[name='quantity']").length > 0){
        quantity = $("#product_list_" + p_id + " .blockWithComplectC input[name='quantity']").val();
    }
    var newPrice = (parseFloat(oldPrice) * area) * quantity;
    var areaPrice = (parseFloat(oldPrice) * area);
    $("#product_list_" + p_id + " .blockWithComplectC .price-new").html('<b>' + newPrice.toFixed(0) + '₽<b>');
    $("#product_list_" + p_id + " .blockWithComplectC input[name='priceArea']").val(areaPrice.toFixed(2));
    $("#product_list_" + p_id + " .blockWithComplectC .ComplectName").html(oldComplectName + ' ' + area + 'м<sup>2</sup>');

}


$(document).on('change', 'select[name^=\'option\']', function () {

    $(this).after('<i class="fa fa-spinner selection-wait"></i>');

    var value = $(this).val();
    var parent_id = $(this).attr('data-parent');

    var select_obj = $(this);

    var poduct_id = $(this).attr('data-product_id');
    $.ajax({
        url: 'index.php?route=product/product/dependentoption&parent_id=' + parent_id + '&value=' + value + '&product_id=' + poduct_id,
        type: 'get',
        dataType: 'json',
        success: function (json) {
            $('.selection-wait').remove();

            if (select_obj.children('option:selected').attr('data-hint') != '') {
                select_obj.next().html(select_obj.children('option:selected').attr('data-hint'));
            }

            if (json['option']) {
                for (i = 0; i < json['option'].length; i++) {
                    if (json['option'][i]['type'] == 'select') {
                        $('#input-option' + json['option'][i]['product_option_id']).stop().fadeOut('medium');

                        $('#input-option' + json['option'][i]['product_option_id']).siblings('.control-label').stop().fadeOut('medium');

                        var html = '';

                        for (j = 0; j < json['option'][i]['option_value'].length; j++) {
                            $('#input-option' + json['option'][i]['product_option_id']).fadeIn('medium');

                            $('#input-option' + json['option'][i]['product_option_id']).siblings('.control-label').fadeIn('medium');

                            html += '<option value="' + json['option'][i]['option_value'][j]['product_option_value_id'] + '">' + json['option'][i]['option_value'][j]['name'];

                            if (json['option'][i]['option_value'][j]['price']) {
                                html += ' (' + json['option'][i]['option_value'][j]['price_prefix'] + json['option'][i]['option_value'][j]['price'] + ')';
                            }

                            html += '</option>';
                        }

                        $('select[name=\'option[' + json['option'][i]['product_option_id'] + ']\']').html(html);
                    } else if (json['option'][i]['type'] == 'radio' || json['option'][i]['type'] == 'checkbox' || json['option'][i]['type'] == 'image') {
                        $('#input-option' + json['option'][i]['product_option_id']).stop().fadeOut('medium');

                        $('#input-option' + json['option'][i]['product_option_id']).siblings('.control-label').stop().fadeOut('medium');

                        $('#input-option' + json['option'][i]['product_option_id']).children().hide();

                        $('#input-option' + json['option'][i]['product_option_id']).find('input').prop('checked', false);

                        for (j = 0; j < json['option'][i]['option_value'].length; j++) {
                            $('#input-option' + json['option'][i]['product_option_id']).fadeIn('medium');

                            $('#input-option' + json['option'][i]['product_option_id']).children().show();

                            $('#input-option' + json['option'][i]['product_option_id']).siblings('.control-label').fadeIn('medium');

                            $('#input-option' + json['option'][i]['product_option_id']).find('input[value=\'' + json['option'][i]['option_value'][j]['product_option_value_id'] + '\']').parent().show();

                            $('#input-option' + json['option'][i]['product_option_id']).find('input[value=\'' + json['option'][i]['option_value'][j]['product_option_value_id'] + '\']').parent().parent().show();
                        }
                    } else {
                        // File, text, textarea, date, datetime
                        if (json['option'][i]['option_value']) {
                            $('#input-option' + json['option'][i]['product_option_id']).parents('.form-group').stop().fadeIn('medium');
                        } else {
                            $('#input-option' + json['option'][i]['product_option_id']).parents('.form-group').stop().fadeOut('medium');
                        }
                    }
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
})

var product_list_cart = {
    'add': function (product_id) {
        console.log(product_id);
        //alert('#product_'+ product_id +' input[type=\'text\'], #product_'+ product_id +' input[type=\'hidden\'], #product_'+ product_id +' input[type=\'radio\']:checked, #product_'+ product_id +' input[type=\'checkbox\']:checked, #product_'+ product_id +' select, #product textarea');
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product_list_' + product_id + ' input[type=\'text\'], #product_list_' + product_id + ' input[type=\'hidden\'], #product_list_' + product_id + ' input[type=\'radio\']:checked, #product_list_' + product_id + ' input[type=\'checkbox\']:checked, #product_list_' + product_id + ' select, #product_list_' + product_id + ' textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#ph-button-cart').button('Добавление');
            },
            complete: function () {
                $('#ph-button-cart').button('Готово');
            },
            success: function (json) {
                console.log(json);
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('.ph-wrap-success').append('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#cart > a').html('<span id="cart-total"><i class="fa fa-shopping-cart" style="color:#f9a51e;" ></i> ' + json['total'] + '</span>');

                    $('html, body').animate({scrollTop: 0}, 'slow');

                    //$('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}