function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}


$(window).load(function () {
    // $('.bf-form').css('opacity',0.1)
    $('.glassesChange').change(function () {
        if ($(this).children('option:selected').attr('data-url') != '') {
            window.location.href = $(this).children('option:selected').attr('data-url');
        }
    });

    if ($('.bf-attr-a20').length > 0) {
        var tmp = $('.bf-attr-a20').closest('.bf-attr-block').html()
        $('.bf-attr-a20').parent().parent().parent().parent().remove()
        $('.bf-form').prepend(tmp)
        $('.bf-form').removeClass('blur');
    } else {
        $('.bf-form').removeClass('blur');
    }
});

$(document).ready(function () {

    $(document).on('click','.button_register',function(){
        $('.login_register_form').show();
    });

    $(document).on('click','.call_back_home',function(){
        $('.call_back_block').show();
    });
    $(".controlLabelImageOption").hover(function () {
        $(this).next().find('.allColor').show();
    }, function () {
        var classH = $(this).next().find('.allColor');
        setTimeout(function () {
            //$(this).next().find('.allColor').hide();
            if (classH.is(':hover')) {
            } else {
                classH.hide();
            }
        }, 400);
    });
    $(document).on('mouseleave', '.allColor', function () {
        $(this).hide();
    });

    function changeColorPogonazh(dataOptionid) {
        $('[class ^= pogonazhLabel]').hide();
        $('[class ^= pogonazhLabel]').find('input').prop("checked", false);

        $('.pogonazhProduct').each(function () {
            if ($(this).find('.pogonazhLabel' + dataOptionid).length > 0) {
                //$(this).find('.pogonazhLabel'+dataOptionid).css('border','1px solid red');
                $(this).find('.pogonazhLabel' + dataOptionid).show();
                $(this).find('.pogonazhLabel' + dataOptionid).find('input').prop("checked", true);
                $(this).find('.pogonazhLabel' + dataOptionid).find('img').removeClass('img-responsive');
                $(this).find('.pogonazhLabel' + dataOptionid).find('img').addClass('img-thumbnail');

                $(this).show();
            } else {
                $(this).hide();
            }
        });
    }


    function checkColor() {
        $('.productColor input').each(function () {
            if ($(this).is(':checked')) {
                if ($(this).parent().data('parentoption_id') == '14') {
                    var dataOptionid = $(this).parent().data('option_id');
                    changeColorPogonazh(dataOptionid);
                }
            }
        });
    }

    checkColor();

    $(document).on('click', '.productColor', function (event) {
        var prod_img = $(this).attr('data-product-image');
        var popup_prod_img = $(this).attr('data-popup-product-image');
        if ($(this).data('parentoption_id') == '14') {
            var dataOptionid = $(this).data('option_id');
            changeColorPogonazh(dataOptionid);
        }
        if (prod_img != '') {
            $("#thumb_img").attr('src', prod_img);
            $("#popup_thumb_img").attr('href', popup_prod_img);
        }
        console.log('checkColor');
        //event.preventDefault();
    });

    $(document).on('click', '.categColor', function (event) {
        var prod_img = $(this).attr('data-product-image');
        var popup_prod_img = $(this).attr('data-popup-product-image')
        product_id = $(this).attr('data-product_id');
        if (prod_img != '') {
            $(".thumb_" + product_id).attr('src', prod_img);
        }
        //event.preventDefault();
    });

    $(document).on('click', '.icon_arrow_pogonazh', function () {
        $(this).parent().next().toggle();
    });


    // $('.pogonazhLabel51').css('border','1px solid red');

    $(document).on('click', '.blockWithComplectC', function () {
        $(this).css('border', '2px solid #fab446');
        $(this).find('h4').css('color', '#fab446');
        $(this).find('.priceChange').css('color', '#fab446');
//        blockWithoutComplectButtonC
        $(this).parent().parent().parent().parent().parent().parent().find("input[name='change-type']").val('complect');

        var blockWithoutComplectC = $(this).parent().parent().parent().find('.blockWithoutComplectC').removeClass('blockComplectArrow');
        $(this).addClass('blockComplectArrow');

        blockWithoutComplectC.children(".stock-box").hide();
        $(this).children(".stock-box").show();

        blockWithoutComplectC.parent().next().css('display', 'none');
        $(this).parent().next().css('display', 'block');

        blockWithoutComplectC.css('border', '2px solid #e2e2e2');
        blockWithoutComplectC.find('h4').css('color', 'black');
        blockWithoutComplectC.find('.priceChange').css('color', 'black');

        $(this).find('[type=checkbox]').prop("checked", true);

        var quantituHtml = blockWithoutComplectC.find('.quantityWithoutComplect').html();
        if (quantituHtml != '') {
            blockWithoutComplectC.find('.quantityWithoutComplect').html('');
            $(this).find('.quantityWithComplect').html(quantituHtml);

            var price = parseFloat(blockWithoutComplectC.find("input[name='old_price']").val());
            blockWithoutComplectC.find(".price .price-new").html("<b>" + price + "₽</b>");

            if ($(this).find('input[name="minQuantity"]')) {
                var min_q = $(this).find('input[name="minQuantity"]').val();
                if (min_q) {
                    //var new_price = parseFloat($(this).find('input[name="old_price"]').val());
                    var area = '';

                    if($(this).parent().parent().parent().parent().parent().parent().find("input[name='area_value']").length > 0){
                        area = parseFloat($(this).parent().parent().parent().parent().parent().parent().find("input[name='area_value']").val());
                    }else{
                        area = 1;
                    }
                    $(this).find('input[name="quantity"]').val(min_q);
                    $(this).find(".price .price-new").html("<b>" + ((price * area) * min_q) + "₽</b>");
                }
            }
        }
    });

    $('.blockWithoutComplectC').click(function () {
        $(this).css('border-color', '#fab446');
        $(this).find('h4').css('color', '#fab446');
        var blockWithComplectC = $(this).parent().parent().parent().find('.blockWithComplectC');

        blockWithComplectC.removeClass('blockComplectArrow');
        $(this).addClass('blockComplectArrow');

        blockWithComplectC.children(".stock-box").hide();
        $(this).children(".stock-box").show();

        $(this).parent().parent().parent().parent().parent().parent().find("input[name='change-type']").val('single');

        blockWithComplectC.parent().next().css('display', 'none');
        $(this).parent().next().css('display', 'block');

        blockWithComplectC.css('border-color', '#e2e2e2');
        blockWithComplectC.find('h4').css('color', 'black');

        blockWithComplectC.find('[type=checkbox]').prop("checked", false);

        var quantituHtml = blockWithComplectC.find('.quantityWithComplect').html();
        if (quantituHtml != '') {
            blockWithComplectC.find('.quantityWithComplect').html('');
            $(this).find('.quantityWithoutComplect').html(quantituHtml);

            var price = parseFloat(blockWithComplectC.find("input[name='old_price']").val());
            //blockWithComplectC.find(".price .price-new").html("<b>" + price + "₽</b>");
        }
    });


    $(document).on('click', '.blockWithComplectP', function () {
        $(this).css('border', '2px solid #fab446');
        $(this).find('h4').css('color', '#fab446');
        //$(this).find('.priceChange').css('color', '#fab446');

        var blockWithoutComplectP = $(this).parent().parent().find('.blockWithoutComplectP');

        blockWithoutComplectP.removeClass('blockComplectArrowP');
        $(this).children(".stock-box").show();
        blockWithoutComplectP.children(".stock-box").hide();
        $(this).addClass('blockComplectArrowP');
        if ($('.blockWithoutComplectPButton').html() != '') {
            $('.blockWithComplectPButton').html($('.blockWithoutComplectPButton').html());
            $('.blockWithoutComplectPButton').html('');
            var price = parseFloat($(".blockWithoutComplectP input[name='priceChange']").val());
            $(".blockWithoutComplectP h2.priceChange").html("<b>" + price + "₽</b>");

        }
        blockWithoutComplectP.css('border', '2px solid #e2e2e2');
        blockWithoutComplectP.find('h4').css('color', 'black');
        blockWithoutComplectP.find('.priceChange').css('color', 'black');

        $(this).find('[type=checkbox]').prop("checked", true);
        $("input[name='change-type']").val('complect');

        var quantituHtml = blockWithoutComplectP.find('.quantityWithoutComplect').html();
        if (quantituHtml != '') {
            blockWithoutComplectP.find('.quantityWithoutComplect').html('');
            $(this).find('.quantityWithComplect').html(quantituHtml);

            var min_q = $(this).parent().parent().find('input[name="minQuantity"]').val();
            var new_price = $(this).find('.priceChange').val();
            var area = 1;
            if($(".partition input[name='area_value']").length > 0){
                area = $(".partition input[name='area_value']").val();
            }
            $(this).find('.quantityWithComplect input[name="quantity"]').val(min_q);
            $(this).find('h2.priceChange').html("<b>" + ((price * min_q) * area).toFixed(0) + "₽</b>");
        }
    });

    $('.blockWithoutComplectP').click(function () {
        $(this).css('border-color', '#fab446');
        $(this).find('h4').css('color', '#fab446');
        var blockWithComplectP = $(this).parent().parent().find('.blockWithComplectP');

        $(this).children(".stock-box").show();
        blockWithComplectP.children(".stock-box").hide();

        blockWithComplectP.removeClass('blockComplectArrowP');
        $("input[name='change-type']").val('single');
        $(this).addClass('blockComplectArrowP');

        if ($('.blockWithComplectPButton').html() != '') {
            $('.blockWithoutComplectPButton').html($('.blockWithComplectPButton').html());
            $('.blockWithComplectPButton').html('');
            var price = parseFloat($(".blockWithComplectP input[name='priceChange']").val());
            //$(".blockWithComplectP h2.priceChange").html("<b>" + price + "₽</b>");
        }
        blockWithComplectP.css('border-color', '#e2e2e2');
        blockWithComplectP.find('h4').css('color', 'black');

        blockWithComplectP.find('[type=checkbox]').prop("checked", false);

        var quantituHtml = blockWithComplectP.find('.quantityWithComplect').html();
        if (quantituHtml != '') {
            blockWithComplectP.find('.quantityWithComplect').html('');
            $(this).find('.quantityWithoutComplect').html(quantituHtml);
        }
    });

    $(".searchButton").hover(function () {
        $('.searchInput').show();
        $('.searchInput').focus();
    }, function () {
        setTimeout(function () {
            if ($('.searchInput').is(':hover')) {
            } else {
                $('.searchInput').hide();
            }
        }, 200);
    });
    $('.searchInput').focusout(function () {
        $('.searchInput').hide();
    });

    $(document).on('change', "input[name='quantity']", function () {
        changePrice($(this).val(), $(this))
    });
    $(document).on('keypress', "input[name='quantity']", function () {
        return false;
    });

    function changePrice(quantity, button) {
        if (button.attr('data-product')) {
            var p_id = button.attr('data-product');
            var price_obj = $(".product-box-" + p_id + ".blockComplectArrowP .priceChange");
            var price = '';
            if($(".product-box-" + p_id + ".blockComplectArrowP input[name='priceArea']").length > 0 && $(".product-box-" + p_id + ".blockComplectArrowP input[name='priceArea']").val() != ''){
                price = parseFloat($(".product-box-" + p_id + ".blockComplectArrowP input[name='priceArea']").val());
            }else {
                price = parseFloat($(".product-box-" + p_id + ".blockComplectArrowP input[name='priceChange']").val());
            }
            var new_price = price * (quantity);
            price_obj.html("<b>" + new_price + "₽</b>");
        }

        if (button.attr('data-pogonazh')) {
            var p_id = button.attr('data-pogonazh');
            var price_obj = $("#product_ser_" + p_id + " .price .price-new");
            var price = parseFloat($("#product_ser_" + p_id + " .price input[name='old_price']").val());
            var new_price = price * (quantity);
            price_obj.html(new_price + "₽");
        }

        if (button.attr('data-service')) {
            var p_id = button.attr('data-service');
            var price_obj = $("#product_ser_" + p_id + " .price .price-new");
            var price = parseFloat($("#product_ser_" + p_id + " .price input[name='old_price']").val());
            var new_price = price * (quantity);
            price_obj.html(new_price + "₽");
        }

        if (button.attr('data-related')) {
            var p_id = button.attr('data-related');
            var price_obj = $(".product-related_" + p_id + " .price .price-new");
            var price = parseFloat($(".product-related_" + p_id + " .price input[name='old_price']").val());
            var new_price = price * (quantity);
            price_obj.html(new_price + "₽");
        }
    }

    function edit_cart_quantityP(prefix, obj, button) {
        var quantity = parseInt(obj.children("input[type='text']").val()),
            id = obj.children("input[type='text']").attr('data-cart_id'),
            product = (button.attr('data-product')) ? button.attr('data-product') : 1,
            minQuantity = $(".product-box-" + product + ".blockComplectArrowP input[name='minQuantity']").val();
        if (!minQuantity) {
            minQuantity = 1;
        }
        switch (prefix) {
            case '-':
            {
                if (quantity > minQuantity) {
                    quantity -= 1;
                }

                changePrice(quantity, button);
                break;
            }
            case '+':
            {
                quantity += 1;
                changePrice(quantity, button);
                break;
            }
        }
        obj.children("input[type='text']").val(quantity);
        //edit_cart_option(id);
    }

    $(document).on('click', '.dec-c-quantityP', function () {

        edit_cart_quantityP('-', $(this).parent().parent(), $(this));
    });
    $(document).on('click', '.inc-c-quantityP', function () {

        edit_cart_quantityP('+', $(this).parent().parent(), $(this));
    });

    $('.inputColor').change(function () {
        $(this).parent().parent().parent().find('img').removeClass('img-thumbnail').addClass('img-responsive');
        $(this).parent().find('img').removeClass('img-responsive').addClass('img-thumbnail');
        $(this).parent().parent().parent().parent().find('.oneColor').find('img').attr('src', $(this).parent().find('img').attr('src'));
    });

    $('#have_questions').click(function(e){
        e.preventDefault();
        $(".have_questions").toggle();
    });

    $(".close_have_questions").click(function(e){
        e.preventDefault();
        $(".have_questions").toggle();
    });


    $(".found_cheaper #phoneValue").mask("+7 (999) 999-9999");
    $(".have_questions #form-phone").mask("+7 (999) 999-9999");

    $(document).on('submit', '#have_questionsForm', function(e){
        e.preventDefault();
        $.ajax({
            url: "index.php?route=product/product/have_questions",
            type: "POST",
            data: $( this ).serialize(),
            cache: false,
            success: function (data) {
                sweetAlert(
                    'Сообщение отправлено!',
                    'Менеджер свяжется с Вами в ближайшее время.',
                    'success'
                );
                $(".have_questions").toggle();
                $("#have_questionsForm").trigger("reset");
            },
            error: function (data) {
                sweetAlert(
                    'Ошибка отправки.',
                    'Свяжитесь с нами любым другим удобным способом.',
                    'error'
                );
            }
        })
    });

    $(document).on('submit', '#foundCheaperForm', function (e) {
        e.preventDefault();
        var fcproduct = $("#productValue").val();
        var fcprice = $("#priceValue").val();
        var fcpriceCheaper = $("#priceCheaper").val();
        var name = $("#nameValue").val();
        var emailcustomer = $("#emailCustomer").val();
        var url_cheaper = $("#url_cheaper").val();
        var phone = $("#phoneValue").val();
        var message = $(".found_cheaper #message").val();
        var firstName = name;
        if (firstName.indexOf(' ') >= 0) {
            firstName = name.split(' ').slice(0, -1).join(' ');
        }
        //console.log({fcproduct, fcprice, name ,emailcustomer, phone, message, url_cheaper, fcpriceCheaper});
        $.ajax({
            url: "index.php?route=product/product/foundCheaper",
            type: "POST",
            data: {
                fcproduct: fcproduct,
                fcprice: fcprice,
                name: name,
                emailcustomer: emailcustomer,
                phone: phone,
                message: message,
                url_cheaper: url_cheaper,
                price_cheaper: fcpriceCheaper
            },
            cache: false,
            success: function () {
                sweetAlert(
                    'Сообщение отправлено!',
                    'Менеджер свяжется с Вами в ближайшее время.',
                    'success'
                );
                $(".you_have_qustion").toggle();

                $('#foundCheaperForm').trigger("reset");
            },
            error: function () {
                sweetAlert(
                    'Ошибка отправки.',
                    'Свяжитесь с нами любым другим удобным способом.',
                    'error'
                );

                $('#foundCheaperForm').trigger("reset");
            },
        })
    })
    $(document).on('click', '#foundCheaper', function (e) {
        e.preventDefault();
        //Наполнение модального окна
        var priceOld = $(this).data('special')
        var product = $(this).data('product')
        var price = $(this).data('price')
        var stock = $(this).data('stock')
        var imgSrc = $(this).data('src')
        var productHref = $(this).data('href')
        console.log([priceOld, product, price, stock, imgSrc])
        $('#productLabel').html(product)
        $('.found_cheaper #thumbfoh').attr('src', imgSrc)
        if (priceOld.length > 0) {
            $("#oldPriceLabel").text(price + '/полотно')
            $('#priceLabel').html(priceOld + '/полотно')
        } else {
            $("#specialPriceLabel").text('')
            $('#priceLabel').html(price + '/полотно')

        }
        $('#productValue').val(product)
        $('#priceValue').val(price)
        // ()
    });

    $('.you_have_qustion_button').click(function () {
        $('.you_have_qustion').toggle();
    });

    $('.buttonChange').click(function () {
        $('.profileText').hide();
        $('.profileForm').show();
    });
    $('.portfolioSaveCancel').click(function () {
        $('.profileText').show();
        $('.profileForm').hide();
    });

    $('.add_address').click(function () {
        $('.add_form').show();
    });

    $('#popular_new_tabs').tab();

    $('#searchS').click(function () {
        $('.mobile_search #searchMobile').toggle();
    });

    $('.box_register_show_one').click(function (e) {
        e.preventDefault();
    });

    $('.mobile_login_click').click(function (e) {
        e.preventDefault();
        $('.login_register_form_one').toggle();
    });
    $('.button_up_icon').click(function () { // При клике по кнопке "Наверх" попадаем в эту функцию
        /* Плавная прокрутка наверх */
        $('body, html').animate({
            scrollTop: 0
        }, 1000);
    });

    //for login show (lg_menu)
    $('.box_register_show').click(function () {
        $('.login_register_form').show();
        $('.login_box').hide();
        $('.register_box').show();
    });

    $('.box_login_show').click(function () {
        $('.register_box').hide();
        $('.login_box').show();
    });

    $(".mouse_hover_login a:first").hover(function () {
        $('.login_register_form').show();
    }, function () {
        setTimeout(function () {
            if ($('.login_register_form').is(':hover')) {
            } else {
                $('.login_register_form').hide();
            }
        }, 200);
    });

    $('.login_register_form').mouseleave(function () {
        $('.login_register_form').hide();
    });


    //call back show - hide
    $(".call_back_q a:first").hover(function () {
        $('.call_back_block').show();
    }, function () {
        setTimeout(function () {
            if ($('.call_back_block').is(':hover')) {
            } else {
                $('.call_back_block').hide();
            }
        }, 200);
    });

    $('.call_back_block').mouseleave(function () {
        $('.call_back_block').hide();
    });
    //end

    $('.call_back_batton').click(function (event) {
        event.preventDefault();
    });

    $('.mobile_call').click(function () {
        $('.call_back_mobile').toggle();
    });


    $(".dropdownL").hover(function () {
        $('.dropdownL').removeClass("dropdownL open").addClass("dropdown");
        $(this).removeClass("dropdown").addClass("dropdownL open");
    }, function () {
        $(this).removeClass("open");
    });

    $(".left_menu>li").hover(function () {
        $(this).css('background-color', '#eee');
    }, function () {
        $(this).css('background-color', 'transparent');
    });

    $(document).on('mouseover', '.product-thumb', function () {
        $(this).find('.product_bottom_info').show();
        $(this).css('box-shadow', '0 0 5px rgba(0,0,0,0.3)');
    });

    $(document).on('mouseout', '.product-thumb', function () {
        $(this).find('.product_bottom_info').hide();
        $(this).css('box-shadow', '0 0 0px rgba(0,0,0,0)');
    });

//        //for login show (xs_menu)
//        $(".mouse_hover_login_one a:first").hover(function(){
//            $('.login_register_form_one').show();
//        },function(){
//                setTimeout(function(){
//                    if ($('.login_register_form_one').is(':hover')) {
//                    }else{
//                        $('.login_register_form_one').hide();
//                    }
//                },200);
//        });
//
//        $('.login_register_form_one').mouseleave(function(){
//            $('.login_register_form_one').hide();
//        });
//
    $('body').on('click', '.box_register_show_one', function () {
        $('.login_register_form_one').show();
        $('.login_box_one').hide();
        $('.register_box_one').show();
    });

    $('.box_login_show_one').click(function () {
        $('.register_box_one').hide();
        $('.login_box_one').show();
    });
    //end


    // Highlight any found errors
    $('.text-danger').each(function () {
        var element = $(this).parent().parent();

        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });

    // Currency
    $('#currency .currency-select').on('click', function (e) {
        e.preventDefault();

        $('#currency input[name=\'code\']').attr('value', $(this).attr('name'));

        $('#currency').submit();
    });

    // Language
    $('#language a').on('click', function (e) {
        e.preventDefault();

        $('#language input[name=\'code\']').attr('value', $(this).attr('href'));

        $('#language').submit();
    });

    /* Search */
    $('#search input[name=\'search\']').parent().find('button').on('click', function () {
        url = $('base').attr('href') + 'index.php?route=product/search';

        var value = $('header input[name=\'search\']').val();

        if (value) {
            url += '&search=' + encodeURIComponent(value);
        }

        location = url;
    });

    $('#search input[name=\'search\']').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('header input[name=\'search\']').parent().find('button').trigger('click');
        }
    });

    // Menu
    $('#menu .dropdown-menu').each(function () {
        var menu = $('#menu').offset();
        var dropdown = $(this).parent().offset();

        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 5) + 'px');
        }
    });

    // Product List
    $('#list-view').click(function () {
        $('#content .product-grid > .clearfix').remove();

        //$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');
        $('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');

        localStorage.setItem('display', 'list');
    });

    // Product Grid
    $('#grid-view').click(function () {
        // What a shame bootstrap does not take into account dynamically loaded columns
        cols = $('#column-right, #column-left').length;

        if (cols == 2) {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
        } else if (cols == 1) {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12');
        } else {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
        }

        localStorage.setItem('display', 'grid');
    });

    $(window).resize(function () {
        if ($(window).width() < 768) {
            $('#grid-view').trigger('click');
        }
    });


    if (localStorage.getItem('display') == 'list') {
        $('#list-view').trigger('click');
    } else {
        $('#grid-view').trigger('click');
    }

    // Checkout
    $(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function (e) {
        if (e.keyCode == 13) {
            $('#collapse-checkout-option #button-login').trigger('click');
        }
    });

    // tooltips on hover
    $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

    // Makes tooltips work on ajax generated content
    $(document).ajaxStop(function () {
        $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
    });
});


var services_cart = {
    'add': function (product_id) {

        $('.serviceForm' + product_id).serializeArray();
        var form_data = $(this).serializeArray();
        $.ajax({
            type: 'POST',
            url: '/index.php?route=information/mail',
            data: {form_data: form_data},
            success: function (response) {
            }
        });
        //alert('#product_'+ product_id +' input[type=\'text\'], #product_'+ product_id +' input[type=\'hidden\'], #product_'+ product_id +' input[type=\'radio\']:checked, #product_'+ product_id +' input[type=\'checkbox\']:checked, #product_'+ product_id +' select, #product textarea');
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product_ser_' + product_id + ' input[type=\'text\'], #product_ser_' + product_id + ' input[type=\'hidden\'], #product_ser_' + product_id + ' input[type=\'radio\']:checked, #product_ser_' + product_id + ' input[type=\'checkbox\']:checked, #product_ser_' + product_id + ' select, #product_ser_' + product_id + ' textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#ph-button-cart').button('Добавление');
            },
            complete: function () {
                $('#ph-button-cart').button('Готово');
            },
            success: function (json) {
                console.log(json);
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('.ph-wrap-success').append('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#cart > a').html('<span id="cart-total"><i class="fa fa-shopping-cart" style="color:#f9a51e;" ></i> ' + json['total'] + '</span>');

                    $('html, body').animate({scrollTop: 0}, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}


// Cart add remove functions
var cart = {
    'add': function (product_id, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                console.log(json);
                $('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart > a').html('<span id="cart-total"><i class="fa fa-shopping-cart" style="color:#f9a51e;" ></i> ' + json['total'] + '</span>');
                    }, 100);
                    if (json['countProducts']) {
                        $('.cart_mobile_icon_2').show();
                        $('.cart_mobile_icon').hide();
                        $('.countProducts').text(json['countProducts']);
                    }
                    $('html, body').animate({scrollTop: 0}, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function (key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function () {

    },
    'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                $('html, body').animate({scrollTop: 0}, 'slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function () {

    }
}

var compare = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({scrollTop: 0}, 'slow');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function () {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function (e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function (data) {
            html = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});

// Autocomplete */
(function ($) {
    $.fn.autocomplete = function (option) {
        return this.each(function () {
            this.timer = null;
            this.items = new Array();

            $.extend(this, option);

            $(this).attr('autocomplete', 'off');

            // Focus
            $(this).on('focus', function () {
                this.request();
            });

            // Blur
            $(this).on('blur', function () {
                setTimeout(function (object) {
                    object.hide();
                }, 200, this);
            });

            // Keydown
            $(this).on('keydown', function (event) {
                switch (event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function (event) {
                event.preventDefault();

                value = $(event.target).parent().attr('data-value');

                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }

            // Show
            this.show = function () {
                var pos = $(this).position();

                $(this).siblings('ul.dropdown-menu').css({
                    top: pos.top + $(this).outerHeight(),
                    left: pos.left
                });

                $(this).siblings('ul.dropdown-menu').show();
            }

            // Hide
            this.hide = function () {
                $(this).siblings('ul.dropdown-menu').hide();
            }

            // Request
            this.request = function () {
                clearTimeout(this.timer);

                this.timer = setTimeout(function (object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }

            // Response
            this.response = function (json) {
                html = '';

                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }

                    // Get all the ones with a categories
                    var category = new Array();

                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }

                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }

                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }

                if (html) {
                    this.show();
                } else {
                    this.hide();
                }

                $(this).siblings('ul.dropdown-menu').html(html);
            }

            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

        });
    }
})(window.jQuery);
