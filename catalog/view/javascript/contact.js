function relMouseCoords(event){
    var totalOffsetX = 0;
    var totalOffsetY = 0;
    var canvasX = 0;
    var canvasY = 0;
    var currentElement = this;

    do{
        totalOffsetX += currentElement.offsetLeft - currentElement.scrollLeft;
        totalOffsetY += currentElement.offsetTop - currentElement.scrollTop;
    }
    while(currentElement = currentElement.offsetParent)

    canvasX = event.pageX - totalOffsetX;
    canvasY = event.pageY - totalOffsetY;

    return {x:canvasX, y:canvasY}
}
HTMLCanvasElement.prototype.relMouseCoords = relMouseCoords;
function decodeEntities(encodedString) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = encodedString;
    return textArea.value;
}
$(document).ready(function () {
    $(document).on('click','.contactHeaderShow',function(){
        $(this).next().toggle();
    });
    
    google.maps.Map.prototype.clearMarkers = function() {
    for(var i=0; i < markerArray.length; i++){
        markerArray[i].setMap(null);
    }
    this.markers = new Array();
};
  var canvas = document.getElementById('canvas');
  var context = canvas.getContext("2d");
  var canvasOffset = $("#canvas").offset();
  var offsetX = canvasOffset.left;
  var offsetY = canvasOffset.top;
  var mapSprite = new Image();
  mapSprite.src = "../../../../image/contact_map.png";
  var Marker = function () {
      this.Sprite = new Image();
      this.Sprite.src = "../../../../image/contact_marker.png"
      this.Width = 24;
      this.Height = 40;
      this.XPos = 0;
      this.YPos = 0;
  }
  var Markers = new Array();
  var mouseClicked = function (e) {

    var x;
    var y;
    if (e.pageX || e.pageY) {
      x = e.pageX;
      y = e.pageY;
    }
    else {
      x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    if($('.menu_block').css('display')!= 'none'){
      x -= $('.menu_block')[0].offsetWidth
    }
    if($('.menu_block2').css('display')!= 'none'){
      y -= $('.menu_block2')[0].offsetHeight
      // x -= $('.menu_block')[0].offsetWidth
    }
    var rect = canvas.getBoundingClientRect();
    var mouseXPos = parseInt(e.x - rect.left);
    var mouseYPos = parseInt(e.y - rect.top);
    for (var i = 0; i < Markers.length; i++) {
        var tempMarker = Markers[i];
          if((((tempMarker.XPos)<mouseXPos)&&((tempMarker.XPos+tempMarker.Width)>mouseXPos))&&((tempMarker.YPos<mouseYPos)&&((tempMarker.YPos+tempMarker.Height)>mouseYPos))){

            var popup = $('.entry'+tempMarker.id).clone()
            popup.attr('id','popup'+tempMarker.id)
            popup.addClass('popup')
            popup.find('.contentInfo').show();
            popup.css('position','absolute')
            popup.css('z-index','999')
            popup.css('min-width','400px')
            popup.css('max-width','500px')
            popup.css('padding','10px 15px 10px 15px')
            popup.css('border','1px solid #f1f1f1')
            popup.css('box-shadow','0 0 10px rgba(0,0,0,0.5)')
            popup.css('-webkit-box-shadow','0 0 10px rgba(0,0,0,0.5)')
            popup.css('-moz-box-shadow','0 0 10px rgba(0,0,0,0.5)')
            popup.prepend('<button type="button" id="'+ tempMarker.id +'" class="cross close" aria-label="Close"><span aria-hidden="true">&times;</span></button>')
            popup.find('.modal-body').remove()
            if(!$('#popup'+tempMarker.id).length){
              $('.prepend').append(popup)
              popup.css('left',(x-(popup[0].offsetWidth/2)))
              popup.css('top',y + tempMarker.Height)
            }

          }
    }
    $('#tip').css('left',x)
    $('#tip').css('top',y)
  }
  $( window ).resize(function() {
    $('.popup').remove()
  });
    canvas.addEventListener("mousedown", mouseClicked, false);

    var firstLoad = function () {
        $.each(aData,function (key,value) {
          var template = $('.template').clone()
          var marker = new Marker()
          marker.name = value.title
          marker.XPos = parseInt(value.xpos)
          marker.YPos = parseInt(value.ypos)
          Markers.push(marker)
          marker.id = Markers.indexOf(marker)
          template.find('.header').text(decodeEntities(value.title))
          template.find('.content').html(decodeEntities(value.description))
          template.find('.map').attr('data-adress',value.adress_db)
          template.removeClass('template')
          template.show()
          template.addClass('entry'+marker.id)
          template.find('#images').attr('id','images'+marker.id)
          template.find('.photos').attr('id','mages'+marker.id)
          if(typeof value.images != 'undefined'){
            $.each(value.images,function (key,value) {
            var img = '<a href="/image/'+ value +'" rel="prettyPhoto['+marker.id+']" title=""><img src="/image/'+ value +'"  width="150" height="150" style="object-fit:contain;margin:6px;"></a>'
              template.find('.modal-body').append(img)
            })
          }
          $('.info').append(template)

          delete template
        })
        context.font = "15px Georgia";
        context.textAlign = "center";
    }

    firstLoad();
    var geocoder = new google.maps.Geocoder()
    var map = new google.maps.Map(document.getElementById('mapGoogle'), {
      zoom: 8,
      center: {lat: -34.397, lng: 150.644}
    });
    google.maps.event.addListenerOnce(map, 'idle', function() {
        google.maps.event.trigger(map, 'resize');
    });
    var pos=""
    var markerArray = []
    $("a[rel^='prettyPhoto']").prettyPhoto({
      overlay_gallery:true,
      ie6_fallback: false,
      social_tools:'',
      gallery_markup: '<div class="pp_gallery"> \
								<div> \
									<ul> \
										{gallery} \
									</ul> \
								</div> \
							</div>',
      // image_markup: '<img id="fullResImage" src="{path}" />',
    });
    var main = function () {
        draw();
    };
    $(document).on('click','.cross',function(){
      $(this).closest('div').remove()
    })
    var draw = function () {
        // Clear Canvas
        context.fillStyle = "#FFF";
        context.fillRect(0, 0, canvas.width, canvas.height);

        // Draw map
        // Sprite, X location, Y location, Image width, Image height
        // You can leave the image height and width off, if you do it will draw the image at default size
        context.drawImage(mapSprite, 0, 0, 700, 700);

        // Draw markers
        for (var i = 0; i < Markers.length; i++) {
            var tempMarker = Markers[i];
            // Draw marker
            context.drawImage(tempMarker.Sprite, tempMarker.XPos, tempMarker.YPos, tempMarker.Width, tempMarker.Height);

            // Calculate postion text
            var markerText = ""//+(tempMarker.name);

            // Draw a simple box so you can see the position
            var textMeasurements = context.measureText(markerText);
            context.fillStyle = "#666";
            context.globalAlpha = 0.7;
            context.fillRect(tempMarker.XPos - (textMeasurements.width / 2), tempMarker.YPos - 15, textMeasurements.width, 20);
            context.globalAlpha = 1;

            // Draw position above
            context.fillStyle = "#000";
            context.fillText(markerText, tempMarker.XPos, tempMarker.YPos);
        }
    };

    setInterval(main, (1000 / 60)); // Refresh 60 times a second
    $(document).on('click','.photos',function () {
      var id = $(this).attr('id')
      $('#i'+id).find("a[rel^='prettyPhoto']").first().trigger('click')
    })
    $(document).on('click','.map',function () {
      var id = $(this).attr('data-adress')
      geocoder.geocode({'address': id}, function(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            pos = results[0].geometry.location
            map.clearMarkers()
            map.setCenter(pos);
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      // $('#map').find('#mapGoogle').html(id)
      $('#map').modal()
    })
    $('#map').on('shown.bs.modal', function(){
      google.maps.event.trigger(map, 'resize');
      // map.clearMarkers()
      var marker = new google.maps.Marker({
        map: map,
        position: pos
      });
      markerArray.push(marker)
      map.setCenter(pos);
      // map.setCenter(new google.maps.LatLng(-33.8688, 151.2195));
    });
})
