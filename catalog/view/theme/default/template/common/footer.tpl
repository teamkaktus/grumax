<style type="text/css">
  .fast-order-thumb {text-align: center;}
  .fast-order-ul {text-align: center;}
  .text-overflow {overflow: hidden; white-space: nowrap; word-wrap: normal; text-overflow: ellipsis;}
</style>

<script type="text/javascript">
jQuery(function($){
   $("#phone").mask("+7 (999) 999-9999");
});
</script>

<script type="text/javascript"><!--
    $(document).on('submit','#contactFormH',function(event) {
     event.preventDefault(); <!--  prevent default submit behaviour
      <!--  get values from FORM
      var foproduct = $("input#foproduct").val();
      var foprice = $("input#foprice").val();
      var fomodel = $("input#fomodel").val();
      var fostock = $("input#fostock").val();
      var name = $("input#name").val();
      var emailcustomer = $("input#emailcustomer").val();
      var phone = $("input#phone").val();
      var message = $("textarea#message").val();
      var firstName = name; <!--  For Success/Failure Message
        <!--  Check for white space in name for Success/Fail message
     if (firstName.indexOf(' ') >= 0) {
    firstName = name.split(' ').slice(0, -1).join(' ');
      }
    $.ajax({
         url: "index.php?route=product/product/fastorder",
         type: "POST",
         data: {foproduct: foproduct, foprice: foprice, fomodel: fomodel, fostock: fostock, name: name, emailcustomer: emailcustomer, phone: phone, message: message},
         cache: false,
         success: function() {
         <!--  Success message
          $('#success').html("<div class='alert alert-success'>");
          $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
         .append( "</button>");
         $('#success > .alert-success')
         .append("Ваш заказ успешно оформлен!<br />Менеджер свяжется с Вами в ближайшее время.");
     $('#success > .alert-success')
     .append('</div>');

     <!-- clear all fields
     $('#contactForm').trigger("reset");
     },
    error: function() {
   <!--  Fail message
    $('#success').html("<div class='alert alert-danger'>");
         $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;")
          .append( "</button>");
         $('#success > .alert-danger').append("Заказ не может быть оформлен.<br />Свяжитесь с нами любым другим удобным способом.");
         $('#success > .alert-danger').append('</div>');
   <!-- clear all fields
   $('#contactForm').trigger("reset");
     }
        })
      })
    $(document).on('click','#callmodal',function(e) {
          e.preventDefault();
          //Наполнение модального окна
          var priceOld = $(this).data('special')
          var product = $(this).data('product')
          var price = $(this).data('price')
          var stock = $(this).data('stock')
          var imgSrc = $(this).data('src')
          var productHref = $(this).data('href')
          $('#fastorderHLabel').html(product)
          $('#thumbfoh').attr('src',imgSrc)
          if (priceOld.length > 0){
            $("#price-old").text(price+'/полотно')
            $('#fopriceh').html(priceOld+'/полотно')
          }else{
            $("#price-old").text('')
            $('#fopriceh').html(price+'/полотно')

          }
          $('#fostock').html(stock)
          $('#foproductHref').html(productHref)
          $('#foproduct').val(product)
          $('#foprice').val(price)
          $("#fastorder_header").modal()
          // ()
    });

  /*When clicking on Full hide fail/success boxes */
  $('#name').focus(function() {
     $('#success').html('');
    });
  <!-- --></script>
<footer class="no_print">
    <div class="row">
        <div class="col-centered" style="width:51px;">
            <button class="button_up_icon" style="height:51px;width:51px;background-color:transparent;border:0px solid transparent;"></button>
        </div>
    </div>
    <div class="row" style="margin-bottom:50px;">
        <div class="col-sm-6">
            <div class="col-lg-8 col-xs-6">
                <p style="text-align:center;font-size:20px;margin-top:30px;">Доставка по всей России</p>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="footer_russia_icon" style="width:150px;height:90px;"></div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-6  col-xs-6">
                <p style="text-align:center;font-size:20px;margin-top:30px;">Принимаем к оплате</p>
            </div>
            <div class="col-sm-6  col-xs-6">
                <div class='icon_yandex_pay pull-left' style='height:30px;width: 50px;margin-right:8px;margin-top:30px;'></div>
                <div class='icon_visa_pay pull-left' style='height:30px;width: 50px;margin-right:8px;margin-top:32px;'></div>
                <div class='icon_masercard_pay pull-left' style='height:30px;width: 50px;margin-top:23px;'></div>
            </div>
        </div>
    </div>
  <div>
    <div class="row">
        <div class="col-md-2 hidden-xs hidden-sm ">
            <div id="logo">
              <?php if ($logo) { ?>
              <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
              <?php } else { ?>
              <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
              <?php } ?>
            </div>
            <p style='text-align:center;margin-top:-18px;'>2001 - <?php echo date('Y'); ?></p>
        </div>
      <div class="col-md-2 col-sm-3 hidden-xs">
        <ul class="list-unstyled">
          <li><a href="/index.php?route=information/howtoorder">Как заказать?</a></li>
          <li><a href="/index.php?route=information/deliverypage">Доставка</a></li>
          <li><a href="/index.php?route=information/paymentpage">Оплата</a></li>
        </ul>
      </div>
      <div class="col-md-2 col-sm-3 hidden-xs">
        <ul class="list-unstyled">
          <li><a href="#">Покупка в кредит</a></li>
          <li><a href="/index.php?route=information/obmenvozvrat">Обмен и возврат</a></li>
          <li><a href="/index.php?route=information/duscontprograma">Дисконтная программа</a></li>
        </ul>
      </div>
      <div class="col-md-2 col-sm-3  hidden-xs">
        <ul class="list-unstyled">
          <li><a href="/index.php?route=information/korporatklient">Корпоративним клиентам</a></li>
          <li><a href="/index.php?route=information/aboutus">О компании</a></li>
          <li><a href="#">Адреса магазинов</a></li>
        </ul>
      </div>
      <div class="col-md-2 col-xs-12 col-sm-3 ">
        <!--<h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>-->
            <div class="footer_map_icon" style="float:left;height:42px;width:48px;"></div>
            <div style="float:left;margin-top:10px;margin-left:5px;"><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></div>
      </div>
      <div class="col-md-2 col-xs-12 hidden-sm">
          <p style="font-size:20px;margin-top:10px;"><?php echo $telephone; ?></p>
      </div>
      <div class="col-xs-12 hidden-lg hidden-md hidden-xs">
          <div class='col-sm-3'>
              <div id="logo">
              <?php if ($logo) { ?>
              <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
              <?php } else { ?>
              <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
              <?php } ?>
            </div>
          </div>
          <div class='col-sm-3 hidden-lg hidden-md hidden-xs'>
              <p style="font-size:20px;margin-top:10px;"><?php echo $telephone; ?></p>
          </div>
          <div class='col-sm-3 hidden-lg hidden-md hidden-xs'>
          </div>
          <div class='col-sm-3 hidden-lg hidden-md hidden-xs'>
          </div>
          </div>
    </div>
    </div>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>
<!--
 <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
      <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
<div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
    </div>
    <hr>
    <p><?php echo $powered; ?></p> -->
