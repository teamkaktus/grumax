<?php echo $header; ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 hidden-xs">
            <div class="text-center" style='position: absolute;
  top: 15px;
  z-index: 2;
  width:40%;
  max-width: 460px;
  min-width: 400px;
  height: 160px;
  overflow: hidden;
  background: #fff;
  margin: 0 auto;
  left: 0;
  right: 0;'>
                <h1>
                    <img src='../../../image/catalog/icon/site_logo.png' style='width:50px;'><br>
                    <?=$heading_title;?>
                </h1>
            </div>
            <img src='../../../image/catalog/imageSpecial.png' style='width:100%;'>
        </div>
        <h1 class='text-center hidden-sm hidden-md hidden-lg'>
            <img src='../../../image/catalog/icon/site_logo.png' style='width:50px;'><br>
            <?=$heading_title;?>
        </h1>
    </div>
    <div class="row" style='padding-top: 30px'>
        <?php if(isset($actions)){
            foreach($actions as $action){
            ?>
                <div class="col-sm-6 col-xs-12 actionHeight" >
                    <div class="col-sm-5 col-md-5 col-lg-4 col-xs-12">
                        <a href="<?=$action['href'];?>">
                            <img src="<?=$action['img'];?>" class='actionImage' alt="<?=$action['title'];?>">
                        </a>
                    </div>
                    <div class="col-sm-7 col-md-7 col-lg-6 col-xs-12">
                        <h3 style='margin-top:0px;'><a href="<?=$action['href'];?>"><?=$action['title'];?></a></h3>
                        <p><?=$action['short_description'];?></p>
                    </div>
                </div>
          <?php  }
        } ?>
    </div>
</div>
<?php echo $footer; ?>