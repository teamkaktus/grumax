<?php  echo $header; ?>
<?php //echo $column_left; ?>
<?php //echo $column_right; ?>
<script>
var aData = <?=$jData?>
</script>
<div id="container" class="prepend" style="margin-left: 15px;margin-right: 15px;background-color:white">
  <div class="row">

    <h1 style="contactHeaderShow">Контакты</h1>

    <div class="col-sm-4 info">
      <div class="template" style="background-color:white; padding-bottom:20px;border-bottom: 1px solid rgb(241, 241, 241);">
          <h2 class="header contactHeaderShow"></h2>
          <div class="contentInfo">
            <div class="content"></div>
            <div class="col-sm-12" style="padding:0px;">
              <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6" style="height:100px;">
                  <img src="../../../../../image/catalog/icon/icon_contact_map.png" class="map" style="width:100%;">
                  <div style="padding:2px 5px 2px 5px;font-size:9px;position:absolute;top:20px;font-size:12px;background-color: #f9a51e;color:black;text-align:center;">
                      карта
                  </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                  <img src="../../../../../image/catalog/icon/icon_contact_salon.png" class="photos" style="width:100%;">
                  <div style="padding:2px 5px 2px 5px;font-size:9px;position:absolute;top:20px;font-size:12px;background-color: #f9a51e;color:black;text-align:center;">
                      фото салона
                  </div>
              </div>
            </div>
          </div>

      <!-- Modal IMAGES -->
      <div id="images" class="modal-body" style="display:none">

      </div>
      <div style="clear:both;"></div>
    </div>

    </div>
    <div class="col-sm-8" style="overflow: hidden">
      <canvas id="canvas" width="700" height="700"></canvas>
    </div>
  </div>
  <div id="map" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="" style="height:500px;width:100%"id="mapGoogle">

        </div>
      </div>
    </div>
  </div>
    <!-- <div id="tip" style="position:absolute;min-width:100px;min-height:25px;z-index:999;background-color:white;border:1px solid black;display:none"></div> -->
</div>
<?php  echo $footer; ?>
