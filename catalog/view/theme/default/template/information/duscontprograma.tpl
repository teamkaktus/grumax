<?php  echo $header; ?>
<?php //echo $column_left; ?>
<?php //echo $column_right; ?>
<style>
    .button_register{
border:1px solid #ec9c05; -webkit-border-radius: 1px; -moz-border-radius: 1px;border-radius: 1px;font-size:12px;font-family:arial, helvetica, sans-serif; padding: 10px 10px 10px 10px; text-decoration:none; display:inline-block;text-shadow: 0px 0px 0 rgba(0,0,0,0.3); color: #FFFFFF;
 background-color: #FBB42E; background-image: -webkit-gradient(linear, left top, left bottom, from(#FBB42E), to(#F88F12));
 background-image: -webkit-linear-gradient(top, #FBB42E, #F88F12);
 background-image: -moz-linear-gradient(top, #FBB42E, #F88F12);
 background-image: -ms-linear-gradient(top, #FBB42E, #F88F12);
 background-image: -o-linear-gradient(top, #FBB42E, #F88F12);
 background-image: linear-gradient(to bottom, #FBB42E, #F88F12);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#FBB42E, endColorstr=#F88F12);
}

.button_register:hover{
 border:1px solid #bf7e04;
 background-color: #f19f05; background-image: -webkit-gradient(linear, left top, left bottom, from(#f19f05), to(#d17406));
 background-image: -webkit-linear-gradient(top, #f19f05, #d17406);
 background-image: -moz-linear-gradient(top, #f19f05, #d17406);
 background-image: -ms-linear-gradient(top, #f19f05, #d17406);
 background-image: -o-linear-gradient(top, #f19f05, #d17406);
 background-image: linear-gradient(to bottom, #f19f05, #d17406);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#f19f05, endColorstr=#d17406);
}

@media (min-width: 300px) and (max-width: 991px) {
    .text_md_align{
        text-align:center;
    }   
}
</style>
<div id="container" style="margin-left: 15px;margin-right: 15px">
    <h1>Дисконтная программа</h1>
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="clear:both;">
            
                <div class="col-sm-12" style='padding:0px;'>
                    <div class="col-sm-3">
                        <div class="col-sm-12 col-xs-12" style='padding:0px;'>
                            <div class="col-sm-10 col-xs-4" style='padding:0px;'>
                                <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_1.png">
                            </div>
                            <div class="col-sm-2 hidden-xs" style='padding:0px;margin-top:32%;'>
                                <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_arrow_1.png">
                            </div>
                            <div class="col-xs-2 hidden-sm hidden-md hidden-lg" style='padding:0px;margin-top:32%;'>
                                <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_arrow_3.png">
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12" style='padding:0px;'>
                            <p>
                                <h4><b>1. Зарегистрируйтесь</b></h4>
                                Чтобы быстрее оформлять заказы и быть в курсе новостей и акций
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="col-sm-12 col-xs-12" style='padding:0px;'>
                            <div class="col-sm-10 col-xs-4" style='padding:0px;'>
                                <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_2.png">
                            </div>
                            <div class="col-sm-2 hidden-xs" style='padding:0px;margin-top:32%;'>
                                <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_arrow_1.png">
                            </div>
                            <div class="col-xs-2 hidden-sm hidden-md hidden-lg" style='padding:0px;margin-top:32%;'>
                                <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_arrow_3.png">
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12" style='padding:0px;'>
                            <p>
                                <h4><b>2. Покупайте</b></h4>
                                Совершите покупку, выбрав из тысяч самых разных товаров
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="col-sm-12 col-xs-12" style='padding:0px;'>
                            <div class="col-sm-10 col-xs-4" style='padding:0px;'>
                                <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_3.png">
                            </div>
                            <div class="col-sm-2 hidden-xs" style='padding:0px;margin-top:32%;'>
                                <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_arrow_1.png">
                            </div>
                            <div class="col-xs-2 hidden-sm hidden-md hidden-lg" style='padding:0px;margin-top:32%;'>
                                <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_arrow_3.png">
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12" style='padding:0px;'>
                            <p>
                                <h4><b>3. Накапливайте</b></h4>
                                Начиная со второй покупки, сумма вашей скидки будет расти       
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="col-sm-12" style='padding:0px;'>
                            <div class="col-sm-10 col-xs-4" style='padding:0px;'>
                                <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_4.png">
                            </div>
                        </div>

                        <div class="col-sm-12 col-xs-12" style='padding:0px;'>
                            <p>
                                <h4><b>4. Получайте скидки</b></h4>
                                Взависимости от общей суммы совершенных покупок       
                            </p>
                        </div>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <div class="col-sm-12" style='margin-top:60px;margin-bottom: 60px;padding:0px;text-align:center;'>
                    <button class='btn button_register' style='padding:4px 56px 0px 56px;border:0px;;'>
                        <h3 style='margin-top:5px;color:white;'>Регистрация</h3>
                    </button>
                </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding:0px;">
            <div style="width:100%;background-size: 100%;padding-bottom:10px;background-image: url('../../../../image/catalog/duskont/duskont_fon_right_column.png')">
                
                <div class='col-sm-4 col-md-12 col-lg-12' style='padding:0px;'>
                    <div class='col-lg-4 col-md-4 text_md_align'>
                        <h1 class="text_md_align" style="font-size:43px;color:#f14b25;"><b>3<sup>%</sup></b></h1>
                    </div>
                    <div class='col-lg-8 col-md-8 col-sm-10 text_md_align' style='padding:0px;'>
                        <h4><b>до 49 999 рублей</b></h4>
                        Начиная со второй покупки и до достижения общей суммы завершённых заказов
                    </div>
                </div>
                
                <div class='col-sm-4 col-md-12 col-lg-12' style='padding:0px;'>
                    <div class='col-lg-4 col-md-4 text_md_align'>
                        <h1 style="font-size:43px;color:#f14b25;"><b>5<sup>%</sup></b></h1>
                    </div>
                    <div class='col-lg-8 col-md-8 text_md_align' style='padding:0px;'>
                        <h4><b>от 100 000 р до 499 999 р</b></h4>
                        Общая сумма заказов в интервале
                    </div>
                </div>
                
                <div class='col-sm-4 col-md-12 col-lg-12' style='padding:0px;'>
                    <div class='col-lg-4 col-md-4 text_md_align'>
                        <h1 style="font-size:43px;color:#f14b25;"><b>7<sup>%</sup></b></h1>
                    </div>
                    <div class='col-lg-8 col-md-8 text_md_align' style='padding:0px;'>
                        <h4><b>от 100 000 р до 499 999 р</b></h4>
                        Максимальная скидка
                    </div>
                </div>
                <div style='clear:both;'></div>
            </div>
        </div>
        <div style='clear:both;'></div>
        <div class="col-sm-12 col-xs-12" style="margin-top:20px;">
            <div class="col-sm-6 col-xs-12" style="padding:0px;">
                <div class="col-sm-2 col-xs-3">
                    <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_arrow_2.png">
                </div>
                <div class="col-sm-8 col-xs-9">
                    Сумма покупок учитывается в системе.  Чем больше сумма, тем больше скидка.
                </div>
            </div>
            <div class="col-sm-6 col-xs-12" style="padding:0px;">
                <div class="col-sm-2 col-xs-3">
                    <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_arrow_2.png">
                </div>
                <div class="col-sm-8 col-xs-9">
                    Сумма первой покупки учитывается и копится, но применить скидку можно, начиная со второй покупки. 
                </div>
            </div>
        </div>
        <div style='clear:both;'></div>
    </div>
</div>
<?php  echo $footer; ?>