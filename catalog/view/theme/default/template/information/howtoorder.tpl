<?php  echo $header; ?>
<div class="row" style="margin-left: 15px;margin-right: 15px">
    <div id="container">
        <h1><?=$heading_title;?></h1>
        <div class="col-sm-12" style="padding: 0">
            <div class="col-sm-4" style="padding: 0">
                <div class="order_box">
                    <div class="col-sm-12" style="padding: 0">
                        <table class="order-table">
                            <tr>
                                <td class="phone-icon">
                                    <img src="/image/catalog/how_to_order/order1.png" alt="По телефону"
                                         class="responsive">
                                </td>
                                <td class="title-text">
                                    <span>По телефону</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-12 order-text">
                        <p>Операторы расскажут о товарах, помогут выбрать и оформят заказ.</p>
                    </div>
                    <div class="col-sm-12 order-body">
                        <div class="col-xs-6" style="min-width: 210px;">
                            <div class="phone">8 495 641-67-64</div>
                            <span class="sub-phone">многоканальный</span>
                        </div>
                        <div class="col-xs-6" style="min-width: 210px;">
                            <div class="phone">8 495 641-67-64</div>
                            <span class="sub-phone">бесплатный звонок</span>
                        </div>
                        <div class="col-xs-12 ">
                            <ul>
                                <li class="order-icon1">Сообщите оператору артикул товара, дату, место и время
                                    доставки.
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-10 col-xs-offset-1">
                            <button onclick="$('.call_back_block').show();">Заказать звонок</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="order_box">
                    <div class="col-sm-12" style="padding: 0">
                        <table class="order-table">
                            <tr>
                                <td class="phone-icon">
                                    <img src="/image/catalog/how_to_order/order2.png" alt="В магазине"
                                         class="responsive">
                                </td>
                                <td class="title-text">
                                    <span>В магазине</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-12 order-text">
                        <p>В розничном магазине 300 моделей дверей.
                            Все можно потрогать, пощупать и выбрать подходящие.</p>
                    </div>
                    <div class="col-sm-12 order-body">
                        <div class="col-xs-12 ">
                            <ul>
                                <li class="order-icon2">Приходите по адресу:<br>
                                    г.Щелково ул. Хотовская, д. 47
                                </li>
                                <li class="order-icon3">Обратитесь к консультантам или используйте электронный каталог
                                    товаров на терминалах
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-10 col-xs-offset-1">
                            <a href="/index.php?route=information/contactpage"><button>Найти на карте</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="order_box">
                    <div class="col-sm-12" style="padding: 0">
                        <table class="order-table">
                            <tr>
                                <td class="phone-icon">
                                    <img src="/image/catalog/how_to_order/order3.png" alt="На сайте" class="responsive">
                                </td>
                                <td class="title-text">
                                    <span>На сайте</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-12 order-text">
                        <p>Заказать товар вы можете на нашем сайте воспользовавшись каталогом</p>
                    </div>
                    <div class="col-sm-12 order-body">
                        <div class="col-xs-12 ">
                            <ul>
                                <li class="order-icon4">Выберите товар в нашем каталоге.</li>
                                <li class="order-icon5">Нажмите кнопку «купить».</li>
                                <li class="order-icon6">кажите данные и место доставки.товаров на терминалах</li>
                                <li class="order-icon7">В течение дня перезвонит оператор.</li>
                            </ul>
                        </div>
                        <div class="col-xs-10 col-xs-offset-1">
                            <a href="/index.php?route=product/category&path=20"><button>Перейти к каталогу</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php echo $footer; ?>
