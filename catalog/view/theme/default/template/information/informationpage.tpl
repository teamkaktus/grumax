<?php  echo $header; ?>
<?php //echo $column_left; ?>
<?php //echo $column_right; ?>
      <div id="container" style="margin-left: 15px;margin-right: 15px">
        <h1>Информация</h1>
        <div class="row information_page_fon_1" style="min-height:230px;">
            <div class="col-sm-12 col-xs-12" style="background-color:white;border:2px solid #efefef;padding-top:10px;padding-bottom: 10px;margin-top:40px;">
                <ul class="nav navbar-nav" style="width:100%;font-size:15px;">
                    <div class="col-sm-2">
                        <li><a href="/index.php?route=information/howtoorder">Оформление заказа</a></li>
                        <li><a href="/index.php?route=information/paymentpage">Оплата заказа</a></li>
                        <li><a href="">Покупка в кредит</a></li>
                    </div>
                    <div class="col-sm-2">
                        <li><a href="/index.php?route=information/deliverypage">Доставка и самовывоз</a></li>
                        <li><a href="">Услуги</a></li>
                        <li><a href="">Гарантия</a></li>
                    </div>
                    <div class="col-sm-2">
                        <li><a href="/index.php?route=information/korporatklient">Корпоративным клиентам</a></li>
                        <li><a href="/index.php?route=information/aboutus">О компании</a></li>
                        <li><a href="">Адреса магазинов</a></li>
                    </div>
                    <div class="col-sm-2">
                        <li><a href="">Вакансии</a></li>
                        <li><a href="/index.php?route=information/action">Акции</a></li>
                        <li><a href="/index.php?route=information/news">Новости</a></li>
                    </div>
                    <div class="col-sm-2">
                        <li><a href="/index.php?route=information/news">Видео</a></li>
                        <li><a href="/index.php?route=information/news">Статьи</a></li>
                    </div>
                </ul>
            </div>
        </div>
        <div class="row" style="margin-bottom: 50px">
            <div class="col-sm-6">
                <div class="col-sm-12 col-xs-12">
                    <div class="information_page_house" style="width:35px;height:35px;float:left;"></div>
                    <h2 style="float:left;margin-top:0px;margin-left: 5px"><a href="#" style="text-decoration: underline">О компании</a></h2>
                </div>
                <div class="col-sm-12 col-xs-12" style="padding-left:50px;">
                    <h5><b>Режим роботи:</b></h5>
                    <?php echo $config_open; ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12 col-xs-12">
                    <div class="information_page_usluga" style="width:35px;height:35px;float:left;"></div>
                    <h2 style="float:left;margin-top:0px;margin-left: 5px"><a href="#"  style="text-decoration: underline">Услуги</a></h2>
                </div>
                <div class="col-sm-12 col-xs-12" style="padding-left:50px;">
                    <?php echo $config_services; ?>
                </div>
            </div>
        </div>
        
        <div class="row information_fon_adressa" style="height:200px;margin-bottom: 50px">
            <div class="col-sm-6 information_icon_russia" style="height:100%;">
                <div class="col-sm-12 col-xs-12">
                    <div class="information_page_dostavka" style="width:35px;height:35px;float:left;"></div>
                    <h2 style="float:left;margin-top:0px;margin-left: 5px"><a href="#" style="text-decoration: underline">Доставка</a></h2>
                </div>
                <div class="col-sm-12 col-xs-12" style="padding-left:50px;">
                    <?php echo $config_delivery; ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12 col-xs-12">
                    <div class="information_page_marker" style="width:25px;height:35px;float:left;"></div>
                    <h2 style="float:left;margin-top:0px;margin-left: 5px"><a href="#"  style="text-decoration: underline">Адреса магазинов</a></h2>
                </div>
                <div class="col-sm-12 col-xs-12" style="padding-left:50px;">
                    <p><?php echo $address; ?></p>
                </div>
            </div>
        </div>
        
        <div class="row" style="margin-bottom: 50px">
            <div class="col-sm-6">
                <div class="col-sm-12 col-xs-12">
                    <div class="information_page_oplata" style="width:35px;height:35px;float:left;"></div>
                    <h2 style="float:left;margin-top:0px;margin-left: 5px"><a href="#" style="text-decoration: underline">Оплата</a></h2>
                </div>
                <div class="col-sm-12 col-xs-12" style="padding-left:50px;">
                    <?php echo $config_payment; ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12 col-xs-12">
                    <div class="information_page_garantia" style="width:25px;height:35px;float:left;"></div>
                    <h2 style="float:left;margin-top:0px;margin-left: 5px"><a href="#"  style="text-decoration: underline">Гарантия</a></h2>
                </div>
                <div class="col-sm-12 col-xs-12" style="padding-left:50px;">
                    <?php echo $config_garant; ?>
                </div>
            </div>
        </div>
            <?php //echo $content_top; ?>
            <?php //echo $content_bottom; ?>
     </div>
<?php echo $footer; ?>