<?php  echo $header; ?>
<?php //echo $column_left; ?>
<?php //echo $column_right; ?>
<div id="container" style="margin-left: 15px;margin-right: 15px">
    <h1><b>Корпоративным клиентам</b></h1>
    <div class="row">
        <div class="col-sm-12 col-lg-9 col-md-9 col-xs-12">
            <div class="col-sm-12 col-xs-12" style="padding:0px;">
                <div class="col-sm-4 col-xs-12">
                    <div class="col-sm-2 col-xs-2" style="padding:4px;">
                        <img style='width:100%;' src="../../../../image/catalog/korporat/korporat_p.png">
                    </div>
                    <div class="col-sm-10 col-xs-10" style="font-size:15px;padding-right:0px;">
                        Профессионально занимаетесь отделочными и ремонтными работами?
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="col-sm-2 col-xs-2" style="padding:4px;">
                        <img style='width:100%;' src="../../../../image/catalog/korporat/korporat_p.png">
                    </div>
                    <div class="col-sm-10 col-xs-10" style="font-size:15px;padding-right:0px;">
                        Ищете партнера на выгодных условиях?
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="col-sm-2 col-xs-2" style="padding:4px;">
                        <img style='width:100%;' src="../../../../image/catalog/korporat/korporat_p.png">
                    </div>
                    <div class="col-sm-10 col-xs-10" style="font-size:15px;padding-right:0px;">
                        Хотите закупать товары для своего предприятия?
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12" style="padding:0px;">
                <h1><b>Лучшие условия для наших корпоративных клиентов</b></h1>
                <div class="col-sm-12" style="padding:0px;">
                    <div class="col-sm-4 col-xs-12">
                        <div class="col-sm-10 col-xs-8">
                            <img style='width:80%;' src="../../../../image/catalog/korporat/korporat_1.png">
                        </div>
                        <div class="col-sm-12 col-xs-12" style="padding:0px;">
                            <h4><b>Поиск отсутствующего товара</b></h4>
                            <h4>Сообщите вашему персональному менеджеру, что необходимо именно вам - мы постараемся найти это для вас</h4>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="col-sm-10 col-xs-8">
                            <img style='width:80%;' src="../../../../image/catalog/korporat/korporat_2.png">
                        </div>
                        <div class="col-sm-12 col-xs-12" style="padding:0px;">
                            <h4><b>Персональный менеджер</b></h4>
                            <h4>За вами будет закреплен персональный менеджер, который обеспечит квалифицированную и своевременную помощь</h4>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="col-sm-10  col-xs-8">
                            <img style='width:80%;' src="../../../../image/catalog/korporat/korporat_3.png">
                        </div>
                        <div class="col-sm-12 col-xs-12" style="padding:0px;">
                            <h4><b>Доставка по всей России</b></h4>
                            <h4>Доставим выбранный товар в любую точку России в кратчайшие сроки</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-3 col-lg-3 col-xs-12" style="padding:0px;">
            <div style="width:100%;background-size: 100%;padding:15px;background-image: url('../../../../image/catalog/duskont/duskont_fon_right_column.png')">
                <h3 style="margin-top:5px;"><b>В пакет корпоративного сервиса входит</b></h3>
                <div class="col-sm-6 col-md-12 col-lg-12 col-xs-12" style="padding:0px;">
                    <div class="col-sm-2 col-xs-2" style="padding:4px;">
                        <img style='width:100%;' src="../../../../image/catalog/korporat/korporat_paket_1.png">
                    </div>
                    <div class="col-sm-10 col-xs-10" style="font-size:15px;">
                        Максимальные скидки и специальные закрытые цены
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-12 col-lg-12 col-xs-12" style="padding:0px;">
                    <div class="col-sm-2 col-xs-2" style="padding:4px;">
                        <img style='width:100%;' src="../../../../image/catalog/korporat/korporat_paket_2.png">
                    </div>
                    <div class="col-sm-10  col-xs-10" style="font-size:15px;">
                        Полный комплекс услуг: подбор интерьерного решения с учётом всех пожеланий
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-12 col-lg-12 col-xs-12" style="padding:0px;">
                    <div class="col-sm-2 col-xs-2" style="padding:4px;">
                        <img style='width:100%;' src="../../../../image/catalog/korporat/korporat_paket_3.png">
                    </div>
                    <div class="col-sm-10  col-xs-10" style="font-size:15px;">
                        Широкий выбор способов оплаты
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-12 col-lg-12 col-xs-12" style="padding:0px;">
                    <div class="col-sm-2 col-xs-2" style="padding:4px;">
                        <img style='width:100%;' src="../../../../image/catalog/korporat/korporat_paket_4.png">
                    </div>
                    <div class="col-sm-10 col-xs-10" style="font-size:15px;">
                        Специальные условия для сотрудников компаний-партнеров
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-12 col-lg-12 col-xs-12" style="padding:0px;">
                    <div class="col-sm-2 col-xs-2" style="padding:4px;">
                        <img style='width:100%;' src="../../../../image/catalog/korporat/korporat_paket_5.png">
                    </div>
                    <div class="col-sm-10 col-xs-10" style="font-size:15px;">
                        Резервирование товара до оплаты
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>