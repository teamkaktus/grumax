<?php  echo $header; ?>
<?php //echo $column_left; ?>
<?php //echo $column_right; ?>
<style>
    .image_li{
        list-style-image: url(../../../../image/catalog/obmen/left_pag.png);
    }
    .image_li li{
        font-size:15px;
    }
</style>
<div id="container" style="margin-left: 15px;margin-right: 15px">
    <h1>Обмен и возврат</h1>
    <div class="row">
        <div class="col-sm-12 col-md-9 col-lg-9">
            <div class="col-sm-12" style="padding:0px;">
                <div class="col-sm-1 hidden-xs">
                    <img style='width:100%;' src="../../../../image/catalog/duskont/duskont_arrow_2.png">
                </div>
                <div class="col-sm-11 col-xs-12" style="font-size:15px;">
                    Уважаемые покупатели, в своей деятельности мы руководствуемся Законом РФ «О защите прав потребителей», «Постановлением Правительства РФ от 19 января 1998 г. № 55» и «ГК РФ», тем самым гарантируя Вам с нашей стороны полное соблюдение Ваших прав как потребителей.
                </div>
            </div>
                <div class="hidden-lg hidden-md col-sm-12 col-xs-12" style="margin-top:20px;padding:0px;">
                    <div style="padding:15px;width:100%;background-size: 100%;padding-bottom:10px;background-image: url('../../../../image/catalog/duskont/duskont_fon_right_column.png')">
                        <h2 style="margin:0px;text-align: center">
                            <b>Время работы отдела рекламаций:</b>
                        </h2>
                        <p style="font-size: 15px;margin:5px 0px 5px 0px;text-align:center;">
                            Понедельник-пятница
                            с 09.00 до 18.00
                        </p>
                        <div class="col-sm-6 col-xs-6" style="padding: 0px">
                            <div class="col-sm-2 col-xs-3" style="padding:5px;">
                                <img style='width:100%;' src="../../../../image/catalog/obmen/telefon.png">
                            </div>
                            <div class="col-sm-10 col-xs-9">
                                <h4>телефон: +7 (495) 276-13-03</h4>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-6" style="padding: 0px">
                            <div class="col-sm-2 col-xs-3" style="padding:5px;">
                                <img style='width:100%;' src="../../../../image/catalog/obmen/mail.png">
                            </div>
                            <div class="col-sm-10 col-xs-9">
                                <h4> e-mail: tovar@lkdjf.ru</h4>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
            <div class="col-sm-12" style="padding:0px;">
                <h1>Как вернуть или обменять товар?</h1>
                <div class="col-sm-4 col-xs-12">
                    <div class="col-sm-10 col-xs-8">
                        <img style='width:100%;' src="../../../../image/catalog/obmen/obmen_3.png">
                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <h4><b>Скачать и заполнить заявление на возврат</b></h4>
                        <div class="col-sm-12 col-xs-12" style="padding:0px;">
                            <div class="col-sm-2 col-xs-2" style="padding-right:5px;">
                                <img style='width:100%;' src="../../../../image/catalog/obmen/save.png">
                            </div>
                            <div class="col-sm-10 col-xs-10" style="padding:0px;">
                                <h4 style="margin-top:5px;">Скачайте бланк заявления</h4>
                                <h4>Внимательно заполните,
                                перепроверьте</h4>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12" style="padding:0px;">
                            <div class="col-sm-2  col-xs-2" style="padding-right:5px;">
                                <img style='width:100%;' src="../../../../image/catalog/obmen/mail_2.png">
                            </div>
                            <div class="col-sm-10 col-xs-10" style="padding:0px;">
                                <p style="font-size:15px;">
                                    Отправьте нам на почту:<br>
                                    dkjhk@jl.ru
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="col-sm-10 col-xs-8">
                        <img style='width:100%;' src="../../../../image/catalog/obmen/obmen_1.png">
                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <h4><b>Вернуть товар </b></h4>
                        <h4>
                            Возврат осуществляется по адресу:
                        г.Москва, пр-д Серебрякова, д.14 стр.15 в любой день с понедельника по пятницу с 09.00 до 18.00.
                        </h4>
                        <h4>
                            При себе необходимо иметь товарный или кассовый чек и документ, удостоверяющий личность
                        </h4>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="col-sm-10 col-xs-8">
                        <img style='width:100%;' src="../../../../image/catalog/obmen/obmen_2.png">
                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <h4><b>Оформить новый заказ или вернуть деньги</b></h4>
                        <h4>
                            Если заказ оплачен банковской картой, деньги вернутся на неё в течение 2-3х дней. Срок возврата зависит от давности размещения заказа и банка, выпустившего карту. Максимальный срок не может превышать 30 дней.
                        </h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" style="padding:0px;">
            <div style="padding:15px;width:100%;background-size: 100%;padding-bottom:10px;background-image: url('../../../../image/catalog/duskont/duskont_fon_right_column.png')">
                <h3 style="margin:0px;"><b>Время работы отдела рекламаций:</b></h3>
                <p style="font-size: 15px;margin:5px 0px 5px 0px;">
                    Понедельник-пятница<br>
                    с 09.00 до 18.00
                </p>
                <div class="col-sm-12" style="padding: 0px">
                    <div class="col-sm-2" style="padding:5px;">
                        <img style='width:100%;' src="../../../../image/catalog/obmen/telefon.png">
                    </div>
                    <div class="col-sm-10">
                        <h4>телефон: +7 (495) 276-13-03</h4>
                    </div>
                </div>
                <div class="col-sm-12" style="padding: 0px">
                    <div class="col-sm-2" style="padding:5px;">
                        <img style='width:100%;' src="../../../../image/catalog/obmen/mail.png">
                    </div>
                    <div class="col-sm-10">
                        <h4> e-mail: tovar@lkdjf.ru</h4>
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12" style="background-color:#f6f3ec;padding-bottom:20px;">
            <h1>Условия обмена</h1>
                        <ul class="image_li">
                            <li>
                                Внешний вид, комплектность товара, а так же соответствие получаемого товара документам, должны быть проверены получателем в момент доставки товара. После получения товара претензии к товарному виду и комплектности товара не принимаются. (Согласно ГК РФ ст.459). При отказе покупателя от заказа в момент передачи товара курьером, стоимость доставки оплачивается в любом случае в полном объеме, за исключением случаев обнаружения брака. покупателя.
                            </li>
                            <li>
                                Для получения дополнительной информации, а также для рассмотрения претензий по качеству товара, просим Вам связаться с нами по телефонам +7 (495) 150 13 10, +7 (800) 555 03 35 и подробно изложить возникшую проблему.
                            </li>
                        </ul>
        </div>
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" style="text-align:center;padding:20px 20px 20px 20px">
            <img style='width:60px;margin:0 auto;' src="../../../../image/catalog/obmen/obmen_smile.png">
            <p>Ваше обращение будет обязательно рассмотрено, мы постараемся найти оптимальное для Вас решение.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 col-md-8 col-sm-8">
            <h1>Возврат и обмен товара надлежащего качества</h1>
            <ul class="image_li">
                <li>
                    Покупатель вправе отказаться от товара в любое время до его передачи (в том числе в момент осмотра приобретаемого товара), а после передачи товара - в течение 14 дней.
                </li>
                <li>
                    Для получения дополнительной информации, а также для рассмотрения претензий по качеству товара, просим Вам связаться с нами по телефонам +7 (495) 150 13 10, +7 (800) 555 03 35 и подробно изложить возникшую проблему.
                </li>
                <li>
                    Возврат товара надлежащего качества возможен в случае, если сохранены его товарный вид, потребительские свойства, а также документ, подтверждающий факт и условия покупки указанного товара.
                </li>
                <li>
                    Отсутствие у потребителя документа, подтверждающего факт и условия покупки товара, не лишает его возможности ссылаться на другие доказательства приобретения товара у данного продавца.
                </li>
                <li>
                    Потребитель не вправе отказаться от товара надлежащего качества, имеющего индивидуально-определенные свойства, если указанный товар может быть использован исключительно приобретающим его потребителем.
                </li>
                <li>
                    Потребитель не вправе отказаться от товаров надлежащего качества, указанных в перечне непродовольственных товаров, не подлежащих возврату или обмену на аналогичный товар других размера, формы, габарита, фасона, расцветки или комплектации (п.8. Мебель бытовая (мебельные гарнитуры и комплекты) (Постановление Правительства РФ от 19.01.1998 N 55)
                </li>
                <li>
                    При отказе потребителя от товара продавец должен возвратить ему денежную сумму, уплаченную потребителем по договору, за исключением расходов продавца на доставку от потребителя возвращенного товара, не позднее чем через десять дней со дня предъявления потребителем соответствующего требования.
                </li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 hidden-xs">
            <img style='width:100%' src="../../../../image/catalog/obmen/vozvrat_zakaz.png">
        </div>
    </div>
    <div class="row hidden-xs">
        <div class="col-lg-9 col-md-8 col-sm-8">
            <h1>Возврат и обмен товара надлежащего качества</h1>
            <ul class="image_li">
                <li>
                    В случае обнаружения потребителем недостатков товара и предъявления требования о его замене продавец (изготовитель, уполномоченная организация или уполномоченный индивидуальный предприниматель, импортер) обязан заменить такой товар в течение семи дней со дня предъявления указанного требования потребителем, а при необходимости дополнительной проверки качества такого товара продавцом (изготовителем, уполномоченной организацией или уполномоченным индивидуальным предпринимателем, импортером) - в течение двадцати дней со дня предъявления указанного требования.
                </li>
                <li>
                    Если у продавца (изготовителя, уполномоченной организации или уполномоченного индивидуального предпринимателя, импортера) в момент предъявления требования отсутствует необходимый для замены товар, замена должна быть проведена в течение месяца со дня предъявления такого требования.
                    Товар ненадлежащего качества должен быть заменен на новый товар, то есть на товар, не бывший в употреблении.
                </li>
                <li>
                    При замене товара гарантийный срок исчисляется заново со дня передачи товара потребителю.
                </li>
                <li>
                    Последствия продажи товара ненадлежащего качества дистанционным способом продажи товара установлены положениями, предусмотренными статьями 18 - 24 Закона РФ «О защите прав потребителей».
                </li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 hidden-xs">
            <img style='width:100%;padding-top:40px;' src="../../../../image/catalog/obmen/tovar_nenad.png">
        </div>
    </div>
    
</div>
    
<?php echo $footer; ?>