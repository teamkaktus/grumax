<div class="row">
    <div class='col-sm-12'>
        <div class="col-sm-4  hidden-xs hidden-sm">
            <h2><a href="<?php echo $all_news_link; ?>" style="text-decoration: underline">Видео</a></h2>
            <?php foreach ($video as $key => $news) { ?>
                <?php if($key == 0){ ?>
                    <div class="col-sm-7" style="margin-bottom:20px; padding-bottom: 5px;">
                        <?php if($news['image'] != ''){ ?>
                            <img src="<?= $news['image']; ?>" style="width:100%;">
                        <?php } ?>
                        <a href="<?php echo $news['view']; ?>" style="text-decoration: underline"><?php echo $news['title']; ?></a><br />
                    </div>
                <?php }else{ ?>
                    <div class="col-sm-5" style="margin-bottom:20px; padding-bottom: 5px;">
                        <?php if($news['image'] != ''){ ?>
                            <img src="<?= $news['image']; ?>" style="width:100%;">
                        <?php } ?>
                        <a href="<?php echo $news['view']; ?>" style="text-decoration: underline"><?php echo $news['title']; ?></a><br />
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="col-sm-4  hidden-xs hidden-sm">
            <h2><a href="<?php echo $all_news_link; ?>" style="text-decoration: underline">Статьи</a></h2>
            <?php foreach ($statia as $key => $news) { ?>
                <?php if($key == 0){ ?>
                    <div class="col-sm-7" style="margin-bottom:20px; padding-bottom: 5px;">
                        <?php if($news['image'] != ''){ ?>
                            <img src="<?= $news['image']; ?>" style="width:100%;">
                        <?php } ?>
                        <a href="<?php echo $news['view']; ?>" style="text-decoration: underline"><?php echo $news['title']; ?></a><br />
                    </div>
                <?php }else{ ?>
                    <div class="col-sm-5" style="margin-bottom:20px; padding-bottom: 5px;">
                        <?php if($news['image'] != ''){ ?>
                            <img src="<?= $news['image']; ?>" style="width:100%;">
                        <?php } ?>
                        <a href="<?php echo $news['view']; ?>" style="text-decoration: underline"><?php echo $news['title']; ?></a><br />
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="col-sm-4">
            <h2><a href="<?php echo $all_news_link; ?>" style="text-decoration: underline">Новости</a></h2>
            <?php foreach ($newsS as $news) { ?>
                <div style="margin-bottom:20px; padding-bottom: 5px;">
                    <span><?php echo $news['date_added']; ?></span><br />
                    <a href="<?php echo $news['view']; ?>" style="text-decoration: underline"><?php echo $news['title']; ?></a><br />
                    <?php echo $news['description']; ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
