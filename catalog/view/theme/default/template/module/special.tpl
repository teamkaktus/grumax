<h3 style='margin:0px;margin-bottom:5px;'><a href="<?php echo $all_special; ?>" style="text-decoration: underline"><?php echo $heading_title; ?></a></h3>
<div class="row">
  <?php foreach ($products as $key => $product) { ?>
  <?php 
    $class = '';
    $class2 = '';
    $color = '';
    switch($key) {
        case 0:
            $class = ' action_fon_home_page ';
            $color = '#464c8d';
            break;
        case 1:
            $class = ' action_fon_home_page_2 ';
            $color = '#8ebde3';
            break;
        case 2:
            $class = ' action_fon_home_page_3 ';
            $color = '#ed703f';
            break;
        case 4:
            $class = ' action_fon_home_page_2 ';
            $class2 = ' hidden-sm hidden-lg ';
            $color = '#8ebde3';
            break;
        case 5:
            $class = 'action_fon_home_page';
            $class2 = ' hidden-sm hidden-lg ';
            $color = '#464c8d';
            break;
        }
  ?>
  <div class="product-layout col-lg-4 col-md-2 col-sm-2 col-xs-12 <?php $class2; ?>" style='padding:5px;'>
          <div class="product-thumb <?php echo $class; ?>" style="margin-bottom: 0px;min-height:200px;">
            <div class="image" style='padding:10px;padding-bottom: 0px'>
                <a href="<?php echo $product['href']; ?>" ><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" style="border-radius: 100%;width:80px;border:6px solid <?php echo $color; ?>;" /></a>
            </div>
            <div>
              <div class="caption" style="padding:10px;min-height: 0px">
                <h5 style='margin:0px;'><a href="<?php echo $product['href']; ?>" style='color:white;'><b><?php echo $product['name']; ?></b></a></h5>
                <div class='col-sm-12 col-xs-12 col-md-12' style="padding:0px;">
                    <?php if ($product['price']) { ?>
                    <p class="price" style='margin-bottom: 5px;margin-top: 10px'>
                      <?php if ($product['special']) { ?>
                        <span class="price-old" style='color:white;margin-left: 0px'><?php echo $product['price']; ?></span>
                        <span class="price-new" style="font-size:15px;color:white;font-weight: bold;"><?php echo $product['special']; ?></span>
                      <?php } ?>
                    </p>
                    <?php } ?>  
                <button type="button" style='width:100%;background-color: #292f7b;margin-bottom:8px;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:white;' onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">В корзину</button>
                </div>
              
              </div>
            </div>
          </div>
        </div>
  <!--<div class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="product-thumb transition">
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
      <div class="caption">
        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
        <p><?php echo $product['description']; ?></p>
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
      </div>
      <div class="button-group">
        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
      </div>
    </div>
  </div> -->
  <?php } ?>
</div>
