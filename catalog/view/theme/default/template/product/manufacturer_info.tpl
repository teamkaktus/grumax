<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul> 
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <h2><?php echo $heading_title; ?></h2>
        <div id="brainyfilter-product-container">
      <?php if ($products) { ?>

              <div class="row">
                <div class="col-sm-2 col-xs-6 text-right">
                  <label class="control-label" for="input-sort" style="color:black;">Сортировать по:</label>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <select id="input-sort" class="changeSelectArrow" onchange="location = this.value;">
                    <?php foreach ($sorts as $sorts) { ?>
                    <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                    <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-sm-2 hidden-xs">
                  <div class="btn-group hidden-xs">
                    <button type="button" id="list-view"  data-toggle="tooltip" title="<?php echo $button_list; ?>" style="outline: none;background-color: transparent !important;border:0px solid transparent;"><i class="fa fa-th-list"></i></button>
                    <button type="button" id="grid-view"  data-toggle="tooltip" title="<?php echo $button_grid; ?>" style="outline: none;background-color: transparent !important;border:0px solid transparent;"><i class="fa fa-th"></i></button>
                  </div>
                </div>
                
                <div class="col-sm-2 hidden-xs text-right">
                  <label class="control-label" for="input-limit">Отображать:</label>
                </div>
                <div class="col-sm-2 hidden-xs text-right">
                  <select id="input-limit" class="changeSelectArrow" onchange="location = this.value;">
                    <?php foreach ($limits as $limits) { ?>
                    <?php if ($limits['value'] == $limit) { ?>
                    <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <br />
              <div class="row">
                <?php foreach ($products as $product) { ?>
                    <div class="product-layout product-grid col-lg-3 col-md-3 col-sm-3 col-xs-12" style="padding-left: 5px;padding-right: 5px">
                        
                        <!--отображения в формате grid -->
                        <div class="product-thumb product_grid">
          <div class="image" style='padding:10px;'>
              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
              <?php if ($product['special']) { ?>
                  <div class='action_product_icon' style='height:50px;width:50px;position:absolute;z-index:99;right:-8px;top:-10px;'></div>
             <?php } ?>
          </div>
            <div class="caption col-sm-12" style="min-height:150px;padding-bottom: 10px;padding-left: 7px;padding-right: 7px">
                    <div class="col-sm-12" style="padding:0px;">
                        <h4 style="margin-bottom: 2px"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                        <div class='col-sm-12' style='padding:0px;'>
                            <?php foreach($product['attribute_groups'] as $product_attribute){
                                    if($product_attribute['attribute_group_id'] == '7'){
                                        foreach($product_attribute['attribute'] as $attrib){
                                            echo '<span style="color:#727272;">'.$attrib['text'].'</span><br />';
                                        };
                                    }
                            }; ?>
                        </div>

                        <div class='col-sm-12' style='padding:0px;'>
                            <?php if (!$product['special']) { ?>
                            <?php } else { ?>
                                <span class="price-old" style='color:#98070a;'><?php echo $product['price']; ?></span>
                            <?php } ?>

                        </div>
                        <div class='col-sm-8 col-xs-8 col-md-7' style="padding:0px;">
                            <?php if ($product['price']) { ?>
                            <p class="price">
                              <?php if (!$product['special']) { ?>
                              <span  style="font-size:15px;font-weight: bold;"><?php echo $product['price']; ?></span><span style="font-size:9px;" >/полотно</span>
                              <?php } else { ?>
                              <span class="price-new" style="font-size:15px;font-weight: bold;"><?php echo $product['special']; ?></span><span >/полотно</span> <!--<span class="price-old"><?php echo $product['price']; ?> </span> -->
                              <?php } ?>
                            </p>
                            <?php } ?>
                        </div>
                        <div class='col-sm-4 col-xs-4 col-md-5' style='padding:0px;'>
                            <?php if (!$product['special']) { ?>
                            <?php if($product['stock'] > 0){ ?>
                                <div style='padding:2px 1px 2px 2px;font-size:9px;background-color: #f9a51e;color:black;text-align:center;'><b>в наличии</b></div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                </div>
            </div>
            <div class="col-xs-12 hidden-lg hidden-lg hidden-sm" >
                <div class="col-sm-12" style="padding-left: 5px;padding-right: 5px">
                    <div class="col-sm-5" style="padding:2px;">
                        <button type="button" style='width:100%;background-color: #f15923;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:white;' onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">В корзину</button>
                    </div>
                    <div class="col-sm-7" style="padding:2px;">
                        <button type="button" id="callmodal" style='width:100%;background-color: #e1e2e6;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:black;' data-product="<?php echo $product['name']; ?>" data-stock="<?=$product['stock']?>" data-price="<?php echo $product['price']; ?>" data-special="<?=$product['special']?>" data-href="<?php echo $product['href']; ?>" data-src="<?php echo $product['thumb']; ?>" >Купить в 1 клик</button>
                    </div>
                </div>
                <div class="col-sm-12" style="padding:0px;padding-top: 10px;padding-bottom: 10px">
                    <div class='col-sm-6 col-xs-6' style='padding:0px;text-align:center;'>
                        <a data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" style="font-size:11px;cursor: pointer" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><div class="header_heart_icon" style="height:25px;width:25px;margin:0 auto;"></div></a>
                    </div>
                    <div class='col-sm-6 col-xs-6' style='padding:0px;'>
                        <a data-toggle="tooltip" style="font-size:11px;cursor: pointer;" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><div data-toggle="tooltip" class="header_vesu_icon" style="height:25px;width:25px;margin:0 auto;"></div></a>
                    </div>
                </div>
            </div>
            
            <div class="product_bottom_info hidden-xs" style='z-index: 100;bottom:-48px;'>
                <div class="col-sm-12" style="padding-left: 5px;padding-right: 5px">
                    <div class="col-sm-5" style="padding:2px;">
                        <button type="button" style='width:100%;background-color: #f15923;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:white;' onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">В корзину</button>
                    </div>
                    <div class="col-sm-7" style="padding:2px;">
                        <button type="button" id="callmodal" style='width:100%;background-color: #e1e2e6;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:black;' data-product="<?php echo $product['name']; ?>" data-stock="<?=$product['stock']?>" data-price="<?php echo $product['price']; ?>" data-special="<?=$product['special']?>" data-href="<?php echo $product['href']; ?>" data-src="<?php echo $product['thumb']; ?>" >Купить в 1 клик</button>
                    </div>
                </div>
                <div class="col-sm-12" style="padding:0px;padding-top: 10px;padding-bottom: 10px">
                    <div class='col-sm-6 col-xs-6' style='padding:0px;text-align:center;'>
                        <a data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" style="font-size:11px;cursor: pointer" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><div class="header_heart_icon" style="height:25px;width:25px;margin:0 auto;"></div></a>
                    </div>
                    <div class='col-sm-6 col-xs-6' style='padding:0px;'>
                        <a data-toggle="tooltip" style="font-size:11px;cursor: pointer;" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><div data-toggle="tooltip" class="header_vesu_icon" style="height:25px;width:25px;margin:0 auto;"></div></a>
                    </div>
                </div>
            </div>
                        </div>
                        <!-- конец grid-->
                        
                        
                        <!--отображения в формате list -->
                        <div class="col-sm-12 product_list" style="border: 1px solid #ddd;margin-bottom: 20px;padding-top: 10px;overflow: auto;" id="product_list_<?= $product['product_id'];?>">
                            
                            <div class="image col-lg-3 col-md-4 col-sm-4" style='padding:10px;'>
                                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
                                <?php if ($product['special']) { ?>
                                    <div class='action_product_icon' style='height:50px;width:50px;position:absolute;z-index:99;right:8px;top:2px;'></div>
                                <?php } ?>
                            </div>
                            <div class="caption col-lg-4 col-md-8 col-sm-8" style="margin-left: 0px;min-height:100px;padding-bottom: 30px;">
                                <div class='col-sm-12' style="padding:0px;">
                                    <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                </div>
                                <div class='col-sm-12' style="padding:0px;">
                                    <?php if ($product['options']) { ?>
                                        <?php foreach ($product['options'] as $option) { ?>
                                            <?php if ($option['type'] == 'select') { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <div class="row">
                                                        <div class='col-sm-4' style="padding:0px;">
                                                          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                        </div>
                                                        <div class='col-sm-8' style="padding-left: 0px">
                                                          <select name="option[<?php echo $option['product_option_id']; ?>]"
                                                        id="input-option<?php echo $option['product_option_id']; ?>" class="changeSelectArrow"
                                                        data-cart_id="<?php echo $product['product_id']; ?>" data-product_id="<?=$product['product_id'];?>" data-parent="<?php echo $option['product_option_id']; ?>">
                                                            <option value=""><?php echo $text_select; ?></option>
                                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                            <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                            </option>
                                                            <?php } ?>
                                                          </select>
                                                        </div>
                                                    </div>
                                            </div>
                                            <?php } ?> 
                                        <?php } ?> 
                                    <?php } ?> 
                                    
                                    <?php if ($product['options']) { ?>
                                                <?php foreach ($product['options'] as $option) { ?>
                                                <?php if ($option['type'] == 'radio') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                  <label class="control-label"><?php echo $option['name']; ?></label>
                                                  <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="radio">
                                                      <label>
                                                        <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                      </label>
                                                    </div>
                                                    <?php } ?>
                                                  </div>
                                                </div>
                                                <?php } ?>
                                                <!--<?php if ($option['type'] == 'checkbox') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                  <label class="control-label"><?php echo $option['name']; ?></label>
                                                  <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="checkbox">
                                                      <label>
                                                        <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                      </label>
                                                    </div>
                                                    <?php } ?>
                                                  </div>
                                                </div>
                                                <?php } ?> -->
                                                <?php if ($option['type'] == 'image') { ?>
                                                    <?php if (!empty($option['product_option_value'])) { ?>
                                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>  cart_<?php echo $product['product_id']; ?>" style="margin-bottom: 5px">
                                                        <div class="row" id="input-option<?php echo $option['product_option_id']; ?>">
                                                            <div class="col-sm-4" style="padding:0px;">
                                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                    <div class="row">
                                                                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                                      <div style='float:left;padding-left:5px;'>
                                                                        <label>
                                                                          <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]"
                                                           value="<?php echo $option_value['product_option_value_id']; ?>"
                                                           data-cart_id="<?php echo $product['product_id']; ?>" data-product_id="<?=$product['product_id'];?>"  class="inputColor" style='display:none;' />
                                                                          <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-responsive"  style='height:27px;'/> 
                                                                          <!--<?php echo $option_value['name']; ?>
                                                                          <?php if ($option_value['price']) { ?>
                                                                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                          <?php } ?>-->
                                                                        </label>
                                                                      </div>
                                                                      <?php } ?>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'text') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'textarea') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                  <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'file') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                  <label class="control-label"><?php echo $option['name']; ?></label>
                                                  <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                                  <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'date') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                  <div class="input-group date">
                                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                    <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                    </span></div>
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'datetime') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                  <div class="input-group datetime">
                                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                    <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                    </span></div>
                                                </div>
                                                <?php } ?>
                                                <?php if ($option['type'] == 'time') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                  <div class="input-group time">
                                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                    <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                    </span></div>
                                                </div>
                                                <?php } ?>
                                                <?php } ?>
                                                <?php } ?>
                                        </div>
                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12">
                                <div class="row">
                                    <div class='col-lg-6' style="padding:0px;">
                                            <div class="col-lg-12 col-md-6 col-sm-6  col-xs-12" style="padding:0px;margin-bottom: 10px">
                                               <div class="col-sm-12 col-xs-12 blockWithoutComplect" style="cursor:pointer;border:2px solid #fab446 !important;">
                                                <h4>Полотно</h4>
                                                <div class="col-sm-12" style="padding:0px;">
                                                <?php if ($product['price']) { ?>
                                                    <p class="price">
                                                      <?php if (!$product['special']) { ?>
                                                      <span  style="font-size:15px;font-weight: bold;"><?php echo $product['price']; ?></span>
                                                      <?php } else { ?>
                                                      <span class="price-new" style="font-size:15px;font-weight: bold;"><?php echo $product['special']; ?></span><span class="price-old"><?php echo $product['price']; ?> </span>
                                                      <?php } ?>
                                                    </p>
                                                    <?php } ?>
                                                </div>
                                                <div class="col-sm-12 quantityWithoutComplect">
                                                    <div class="input-group parent_block_cart">
                                                        <span class="input-group-btn">
                                                            <button class="dec-c-quantityC btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent; border-radius: 0px">-</button>
                                                        </span>
                                                        <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control" style="text-align: center;border: 0px solid transparent;box-shadow: none;" />
                                                        <span class="input-group-btn">
                                                            <button class="inc-c-quantityC btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent;border-radius: 0px">+</button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <p>Включает полотно</p>
                                            </div>
                                            </div>

                                            <?php if ($product['options']) { ?>
                                            <?php foreach ($product['options'] as $option) { ?>
                                                <?php if ($option['type'] == 'checkbox') { ?>
                                                 <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12" style="padding:0px;margin-bottom: 10px">
                                                 <div class="col-sm-12 col-xs-12 blockWithComplect" style="cursor:pointer;border: 2px solid #e2e2e2;">
                                                         <?php if ($product['options']) { ?>
                                                         <?php $optionPrice = ''; ?>
                                            <?php foreach ($product['options'] as $option) { ?>
                                                <?php if ($option['type'] == 'checkbox') { ?>
                                                                     <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                                      <?php if ($option_value['price']) { ?>
                                                                         <?php $optionPrice  = $option_value['price']; ?>
                                                                         <?php } ?>
                                                                     <?php } ?>
                                                             <?php } ?>
                                                         <?php } ?>
                                                     <?php } ?>
                                                     <h4>Комплект</h4>
                                                     <div class="col-sm-12" style="padding:0px;">
                                                        <?php if ($product['price']) { ?>
                                                        <p class="price">
                                                          <?php if (!$product['special']) { ?>
                                                          <span  style="font-size:15px;font-weight: bold;"><?php echo $product['price'] + $optionPrice; ?></span>
                                                          <?php } else { ?>
                                                          <span class="price-new" style="font-size:15px;font-weight: bold;"><?php echo $product['special'] + $optionPrice; ?></span> <span class="price-old"><?php echo $product['price']; ?> </span>
                                                          <?php } ?>
                                                        </p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-sm-12  quantityWithComplect"></div>
                                                    <!--<div class="input-group parent_block_cart">
                                                         <span class="input-group-btn">
                                                             <button class="dec-c-quantityP btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent; border-radius: 0px">-</button>
                                                         </span>
                                                         <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control" style="text-align: center;border: 0px solid transparent;box-shadow: none;" />
                                                         <span class="input-group-btn">
                                                             <button class="inc-c-quantityP btn btn-secondary" type="button" style="background-color: #e0e0e0 !important;border:0px solid transparent;border-radius: 0px">+</button>
                                                         </span>
                                                    </div>-->
                                                     <div class="col-sm-12" style="padding:0px;">
                                                     <?php if ($product['options']) { ?>
                                            <?php foreach ($product['options'] as $option) { ?>
                                                <?php if ($option['type'] == 'checkbox') { ?>
                                                                 <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                                   <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                                     <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                                     <div class="checkbox">
                                                                       <label style="padding:0px;">
                                                                         <input type="checkbox"  style="display:none;" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                         <?php echo $option_value['name']; ?>
                                                                       </label>
                                                                     </div>
                                                                     <?php } ?>
                                                                   </div>
                                                                 </div>
                                                             <?php } ?>
                                                         <?php } ?>
                                                     <?php } ?>
                                                     </div>    
                                                 </div>
                                                 </div>
                                                 <?php } ?>
                                                 <?php } ?>
                                             <?php } ?>
                                             </div>
                                            <div class='col-lg-6 col-md-12 col-sm-12' style="padding:0px;">
                                                    <div class="hidden-lg col-md-6 col-sm-6" style="padding:5px;padding-top: 10px;padding-bottom: 10px">
                                                        <div class='col-lg-12 col-md-6 col-sm-6'>
                                                            <a data-toggle="tooltip" style="font-size:11px;cursor: pointer;" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');">
                                                                <div data-toggle="tooltip" class="header_vesu_icon" style="height:20px;width:20px;float:left;"></div>
                                                                <h5 style='margin-left: 5px;float:left;margin-top: 5px;color:#387ab0;'>Сравнить</h5></a>
                                                        </div>
                                                        <div class='col-lg-12 col-md-6 col-sm-6'>
                                                            <a data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" style="font-size:11px;cursor: pointer" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                                                                <div class="header_heart_icon" style="height:20px;width:20px;float:left;"></div>
                                                                <h5 style='margin-left: 5px;float:left;margin-top: 5px;color:#387ab0;'>В избранное</h5>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-6 col-sm-6">
                                                        <div class="col-lg-12 col-md-6 col-sm-6" style="padding:2px;">
                                                            <input type="hidden" name="product_id" value="<?= $product['product_id'];?>">
                                                            <button type="button" style='width:100%;background-color: #f15923;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:white;'  onclick="product_list_cart.add('<?php echo $product['product_id']; ?>');">В корзину</button>
                                                        </div>
                                                        <div class="col-lg-12 col-md-6 col-sm-6" style="padding:2px;">
                                                            <button type="button" id="callmodal" style='width:100%;background-color: #e1e2e6;border:0px solid transparent;padding-top: 5px;padding-bottom: 5px;color:white;' data-product="<?php echo $product['name']; ?>" data-stock="<?=$product['stock']?>" data-price="<?php echo $product['price']; ?>" data-special="<?=$product['special']?>" data-href="<?php echo $product['href']; ?>" data-src="<?php echo $product['thumb']; ?>" >Купить в 1 клик</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 hidden-md hidden-sm hidden-xs" style="padding:5px;padding-top: 10px;padding-bottom: 10px">
                                                        <div class='col-lg-12 col-md-6 col-sm-6'>
                                                            <a data-toggle="tooltip" style="font-size:11px;cursor: pointer;" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');">
                                                                <div data-toggle="tooltip" class="header_vesu_icon" style="height:20px;width:20px;float:left;"></div>
                                                                <h5 style='margin-left: 5px;float:left;margin-top: 5px;color:#387ab0;'>Сравнить</h5></a>
                                                        </div>
                                                        <div class='col-lg-12 col-md-6 col-sm-6'>
                                                            <a data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" style="font-size:11px;cursor: pointer" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                                                                <div class="header_heart_icon" style="height:20px;width:20px;float:left;"></div>
                                                                <h5 style='margin-left: 5px;float:left;margin-top: 5px;color:#387ab0;'>В избранное</h5>
                                                            </a>
                                                        </div>
                                                    </div>
                                            </div>
                                </div>
                            </div>    
                        </div>
                        <!-- конец grid-->
                        
                      </div>
                <?php } ?>
              </div>
              <div class="row">
                <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                <div class="col-sm-6 text-right"><?php echo $results; ?></div>
              </div>
              <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>