<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'> -->

<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
<link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet" media="screen" />
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic"
rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/geoip.css" rel="stylesheet">
<script src="catalog/view/javascript/jquery/jqBootstrapValidation.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery.maskedinput.js" type="text/javascript"></script>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery.geoip-module.js" type="text/javascript"></script>
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>

</head>
<body class="<?php echo $class; ?>">
    <div class="col-lg-2 col-md-3 hidden-sm hidden-xs no_print menu_block">

       <div class="affix" style="z-index: 101">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>

        <div class="list-group">
            <ul class="nav left_menu">
            <?php foreach ($categories as $category) { ?>
                <?php if ($category['children']) { ?>
                <li class="dropdown dropdownL" style="padding:10px;"><image style="height:30px;" src="<?php echo $category['image']; ?>"></image><a style="margin-left:5px;font-size:14px;display:initial;padding:0px;border:0px;" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                    <!--<div class="dropdown-menu">-->
                    <!--<div class="dropdown-inner">-->
                      <ul class="dropdown-menu" style="padding-left:0px;margin-left: 100%;margin-top:-50px;">
                      <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                        <?php foreach ($children as $child) { ?>
                        <li style="display:block;height:38px;"><a style="width:100% !important;border:0px;" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                        <?php } ?>
                      <?php } ?>
                        <li><a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a></li>
                      </ul>
                    <!--</div>-->
                  <!--</div>-->
                </li>
                <?php } else { ?>
                    <li  style="padding:10px;"><image style="height:30px;" src="<?php echo $category['image']; ?>"></image><a style="margin-left:5px;font-size:14px;display:initial;padding:0px;border:0px;" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                <?php } ?>
                <?php } ?>
            </ul>
        </div>
       </div>


    </div>
    <div class="hidden-sm hidden-md hidden-lg col-xs-12 no_print menu_block2" style="border-left: 1px solid #e2e2e2;">
        <ul class="list-inline">
            <li class="pull-left">
                <div id="logo">
                  <?php if ($logo) { ?>
                  <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
                  <?php } else { ?>
                  <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                  <?php } ?>
                </div>
            </li>
            <li class="pull-right">
                <a href="#"><div class="mobile_call" style="height:55px;width:55px;margin-top: 5px"></div></a>
            </li>
        </ul>
    </div>

    <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12 print_left_border" style="border-left: 1px solid #e2e2e2;">
        <div class="mobile_version no_print">
                <nav class="navbar navbar-default hidden-sm hidden-md hidden-lg" style="background-color: #63a3d8;margin-bottom: 0px">
                    <div class="navbar-header pull-left">
                      <a class="collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="border:0px solid transparent;">
                        <span class="sr-only">Toggle navigation</span>
                        <span style="color:white;">Меню <span class="fa fa-sort-desc"></span></span>
                      </a>
                    </div>
                        <ul class="nav navbar-nav mobile_menu_header" style="margin:0px;">
                                    <li class="pull-right"><?php echo $cart; ?></li>
                                    <li class=" pull-right mouse_hover_login_one"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="mobile_login_click" style="margin-top: 9px"><div class="key_mobile_icon_2" style="height:15px;width:15px;"></div> <span class="hidden-xs hidden-sm hidden-md">Войти</span><!--<span class="caret">--></span></a>
                                    </li>
                                    <li class="pull-right"><?php echo $search; ?></li>
                        </ul>
                      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"  style="background-color: white;">
                        <ul class="nav navbar-nav"  class="hidden-lg hidden-md hidden-xs">
                                <?php foreach ($categories as $category) { ?>
                                <?php if ($category['children']) { ?>
                                <li class="dropdown" style="padding:10px;"><image style="height:30px;" src="<?php echo $category['image']; ?>"></image><a style="margin-left:5px;font-size:12px;display:initial;padding:0px;border:0px;" href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
                                  <div class="dropdown-menu">
                                    <div class="dropdown-inner">
                                      <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                      <ul class="list-unstyled">
                                        <?php foreach ($children as $child) { ?>
                                        <li style="display:block;height:38px;"><a style="width:100% !important;border:0px;" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                        <?php } ?>
                                      </ul>
                                      <?php } ?>
                                    </div>
                                    <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
                                </li>
                                <?php } else { ?>
                                <li  style="padding:10px;"><image style="height:30px;" src="<?php echo $category['image']; ?>"></image><a style="margin-left:5px;font-size:12px;display:initial;padding:0px;border:0px;" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                <?php } ?>
                                <?php } ?>
                            </ul>
                    </div>
                </nav>
                <div class="mobile_search hidden-lg hidden-md hidden-sm">
                    <?php echo $search; ?>
                </div>
                <div class="login_register_form_one hidden-lg hidden-md hidden-sm" style="margin-top:5px;">
                    <div class="login_box_one" style='padding-top: 10px;padding-bottom: 15px'>
                        <p style="font-size:15px;text-align:center;color:black;text-shadow: 0px 0px 0px black, 0 0 0em black;">Войдите в личний кабинет</hp>
                        <form action="<?php echo $login_form_action; ?>" method="post" enctype="multipart/form-data">
                            <div  style="margin:0 auto;width:180px;">
                                <input type="text" name="email" class="form-control" style="margin-bottom:10px;width:100%;" placeholder="Логин" id="input-email" />
                                <input type="password" name="password" class="form-control" style="width:100%;"  placeholder="Пароль" id="input-password" />
                                <a href="" style="color:#383841;text-shadow:none">Забили пароль?</a>
                            </div>
                            <div  style="margin:0 auto;width:175px;">
                                <input type="submit" class="form-control pull-left" style="margin-right: 5px;border:0px solid transparent;background-color:#292f7b;color:white;width:70px;margin-bottom:10px;" value="Войти"  />
                        </form>
                                <button type="submit" class="box_register_show_one form-control  pull-left" style="border:0px solid transparent;background-color:#ea5b23;color:white;width:100px;">Регистрация</button>
                            </div>
                        </div>
                    <div class="register_box_one" style="height:200px;display:none;">
                            <a href="<?php echo $register; ?>"><?php echo $text_register; ?></a>
                            <div style="margin:0 auto;margin-top:10px;width:90px;">
                                <input type="submit" style="border:0px solid transparent;background-color:#292f7b;color:white;width:90px;margin-bottom:10px;" value="Регистрация"  />
                            </div>
                            <div style="margin:0 auto;margin-top:10px;width:120px;">
                                <button type="submit" class="box_login_show_one" style="border:0px solid transparent;background-color:#ea5b23;color:white;width:120px;">Вход с паролем</button>
                            </div>
                    </div>
                </div>
                <div class="call_back_mobile hidden-lg hidden-md hidden-sm">
                    <iframe src="./index.php?route=module/callme/open&amp;prod_id=" width="260" height="300" frameborder="0" style='display:block;margin:0 auto;'>Загрузка</iframe>
                </div>
        </div>
                <nav id="top" class="hidden-xs  navbar-fixed-top no_print" style="z-index: 100;margin-bottom: 0px;margin-top: 0px;padding-top:0px">
                    <div class="col-lg-2 col-md-3">

                    </div>
                    <div class="col-lg-10 col-md-9" style="background-color: white;padding-top:10px;border-left: 1px solid #e2e2e2;border-bottom: 1px solid #e2e2e2;">
                      <?php //echo $currency; ?>
                      <?php //echo $language; ?>
                      <div id="top-links" class="nav">
                        <ul class="list-inline">
                          <li style="float:left;" class="hidden-lg hidden-md hidden-xs">
                              <div id="logo" style="margin:0px;margin-top: -10px">
                                <?php if ($logo) { ?>
                                <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
                                <?php } else { ?>
                                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                                <?php } ?>
                              </div>
                          </li>
                          <li style="float:left;" class='hidden-xs'><?php echo $geoip; ?></li>
                          <li style="float:left;"><?php echo $search; ?></li>
                          <li class='call_back_q'>
                              <a style="font-size: 20px;border-bottom:1px dashed #387ab0;color:#387ab0;" class="hidden-xs call_back_batton">
                                  <?php echo $telephone; ?>
                              </a>
                              <div class='block'>
                                  <div class='call_back_block'>
                                      <iframe src="./index.php?route=module/callme/open&amp;prod_id=" width="280" height="300" frameborder="0">Загрузка</iframe>
                                  </div>
                              </div>
                          </li>
                          <li class="dropdown pull-right <?php if(!$logged){ echo 'mouse_hover_login'; }?>">
                              <?php if($logged){ ?>
                                      <a  class="dropdown-toggle" data-toggle="dropdown" style="cursor:pointer;"><i class="fa fa-user" style="color:#f9a51e;"></i><?php echo $text_account; ?></a>
                                  <?php }else{ ?>
                                      <a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><div class="login_icon_header" style="opacity:0.9;margin-top:4px;height:12px;width: 11px;float:left;"></div><span class="hidden-xs hidden-sm hidden-md" style="margin-left:2px;color:black;">Войти</span></a>
                                  <?php } ?>

                          <?php if ($logged) { ?>
                            <ul class="dropdown-menu dropdown-menu-right">
                              <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                              <!--<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                              <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                              <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li> -->
                              <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                              <!--<li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                              <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li> -->
                            </ul>
                          <?php } else { ?>
                              <?php } ?>
                              <div class="login_register_form">
                                  <div class="login_box">
                                      <p style="font-size:15px;text-align:center;color:black;text-shadow: 0px 0px 0px black, 0 0 0em black;">Войдите в личний кабинет</hp>
                                      <form action="<?php echo $login_form_action; ?>" method="post" enctype="multipart/form-data">
                                          <div  style="margin:0 auto;width:140px;">
                                              <input type="text" name="email" style="margin-bottom:10px" placeholder="Логин" id="input-email" />
                                              <input type="password" name="password"  placeholder="Пароль" id="input-password" />
                                              <a href="" style="color:#383841;text-shadow:none">Забили пароль?</a>
                                          </div>
                                          <div style="margin-left:25px;margin-top:10px;">
                                              <input type="submit" style="border:0px solid transparent;background-color:#292f7b;color:white;width:70px;margin-left:80px;margin-bottom:10px;" value="Войти"  />
                                          </div>
                                      </form>
                                          <div style="margin-left:25px;margin-top:10px;">
                                              <button type="submit" class="box_register_show" style="border:0px solid transparent;background-color:#ea5b23;color:white;width:100px;margin-left:65px;">Регистрация</button>
                                          </div>
                                  </div>
                                  <div class="register_box" style="height:200px;display:none;">
                                      <p style="font-size:15px;text-align:center;color:black;text-shadow: 0px 0px 0px black, 0 0 0em black;">Регистрация</hp>
                                      <form action="<?php echo $register_form_action; ?>" method="post" enctype="multipart/form-data">
                                          <div  style="margin:0 auto;width:140px;">
                                              <input type="text" name="email" style="margin-bottom:10px" placeholder="Логин" id="input-email" />
                                              <input type="password" name="password"  placeholder="Пароль" id="input-password" />
                                              <a href="" style="color:#383841;text-shadow:none">Забили пароль?</a>
                                          </div>
                                          <div style="margin-left:25px;margin-top:10px;">
                                              <input type="submit" style="border:0px solid transparent;background-color:#292f7b;color:white;width:90px;margin-left:60px;margin-bottom:10px;" value="Регистрация"  />
                                          </div>
                                      </form>

                                          <div style="margin-left:25px;margin-top:10px;">
                                              <button type="submit" class="box_login_show" style="border:0px solid transparent;background-color:#ea5b23;color:white;width:120px;margin-left:45px;">Вход с паролем</button>
                                          </div>
                                  </div>
                              </div>
                          </li>
                          <li class="pull-right" style="color:black;"><?php echo $cart; ?></li>
                          <?php if ($logged) { ?>
                          <li class="pull-right hidden-xs"><a href="" style="color:black;border-bottom:1px solid #f9a51e;margin-left: 2px;margin-right: 5px"><div class='header_vesu_icon' style='height:18px;width:17px;float:left;margin-top:3px;'></div>Список сравнения(<span style="color:#f9a51e;"> <?= $compare_count; ?> </span>)</a></li>
                          <li  class="pull-right  hidden-xs"><a href="" style="color:black;border-bottom:1px solid #f9a51e;margin-left: 2px;margin-right: 5px"><div class='header_heart_icon' style='height:17px;width:16px;float:left;margin-top:3px;'></div>Избранное(<span style="color:#f9a51e;"> <?= $wishlist_count; ?> </span>)</a></li>
                          <?php } ?>
                          <li class="pull-right  hidden-xs"><a href="<?php echo $contact_page_link; ?>" style="color:black;border-bottom:1px solid #f9a51e;margin-left: 2px;margin-right: 5px">Контакты</a></li>
                          <li class="pull-right  hidden-xs"><a href="<?php echo $information_page_link; ?>" style="color:black;border-bottom:1px solid #f9a51e;margin-left: 2px;margin-right: 5px">Информация</a></li>
                          <li class="pull-right  hidden-xs hidden-sm"><a href="<?php echo $payment_page_link; ?>" style="color:black;border-bottom:1px solid #f9a51e;margin-left: 2px;margin-right: 5px">Оплата</a></li>
                          <li class="pull-right  hidden-xs hidden-sm"><a href="<?php echo $delivery_page_link; ?>" style="color:black;border-bottom:1px solid #f9a51e;margin-left: 2px;margin-right: 5px">Доставка</a></li>
                          <li class="pull-right  hidden-xs hidden-sm"><a href="/index.php?route=information/information&information_id=7" style="color:black;border-bottom:1px solid #f9a51e;margin-left: 2px;margin-right: 5px">Как заказать?</a></li>
                          <!--<li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
                          <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
                          <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>-->
                        </ul>
                      </div>
                    </div>
                </nav>
<header class="no_print">
  <div>
    <div class="row" style="margin-top:70px;">
        <div class="hidden-lg hidden-md col-sm-12 hidden-xs" align="center" style="text-align:center;border:1px solid #e2e2e2;">
            <ul class="nav navbar-nav" style="width: 100%">
                <div class="container">
                        <?php foreach ($categories as $category) { ?>
                        <?php if ($category['children']) { ?>
                        <li class="dropdown" style="display: inline-block;padding:10px;"><image style="height:30px;" src="<?php echo $category['image']; ?>"></image><a style="margin-left:5px;font-size:12px;display:initial;padding:0px;border:0px;" href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
                            <!--<div class="dropdown-menu">-->
                                <!--<div class="dropdown-inner">-->
                                  <ul class="dropdown-menu" style="padding-left:0px;">
                                  <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                    <?php foreach ($children as $child) { ?>
                                    <li style="display:block;height:38px;"><a style="width:100% !important;border:0px;" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                    <?php } ?>
                                  <?php } ?>
                                    <li><a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a></li>
                                  </ul>
                                <!--</div>-->
                              <!--</div>-->
                        </li>
                        <?php } else { ?>
                        <li  style="padding:10px;display: inline-block;"><image style="height:30px;" src="<?php echo $category['image']; ?>"></image><a style="margin-left:5px;font-size:12px;display:initial;padding:0px;border:0px;" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                        <?php } ?>
                        <?php } ?>
                </div>
            </ul>
        </div>
      <!--<div class="col-sm-4">
        <div id="logo">
          <?php //if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php //} else { ?>
          <h1><a href="<?php echo $home; ?>"><?php //echo $name; ?></a></h1>
          <?php //} ?>
        </div>
      </div>-->
    </div>
  </div>
</header>
<?php if ($categories) { ?>
<!--<div>
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div> -->
<?php } ?>
<!--Modal-->
              <div class="modal fade no_print" id="fastorder_header" tabindex="-1" role="dialog" aria-labelledby="fastorderLabel">
                <div class="modal-dialog modal-lg" role="document">

                <!-- StartModalContent -->
                <div class="modal-content">

                  <!-- ModalHeadStart -->
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title text-overflow">Купить в один Клик</h4>
                  </div>
                  <!-- ModalHeadEnd -->

              <!-- StartModalBody -->
              <div class="modal-body">

              <!-- FormStart -->
              <form name="sentMessage" id="contactFormH">
              <div class="container-fluid">
                <div class="row">
                <div class="col-md-12">
                  <div class="row">

                  <!-- StartProductInfo -->
                  <div class="col-md-6">
                    <div class="row">

                    <!-- StartProductImage -->
                    <div class="col-md-6 fast-order-thumb">
                      <img class="img-responsive" id="thumbfoh" src="">
                    </div>
                    <!-- EndProductImage -->

                    <!-- StartProductUL -->
                    <div class="col-md-6 fast-order-ul">
                      <h4 class="modal-title text-overflow" id="fastorderHLabel"></h4>
                                  <ul class="list-unstyled">
                                    <li>
                                    <p id="fopriceh"></p>
                                    </li>

                                        <span style="text-decoration: line-through;" id="price-old"></span>
                                    </li>
                                    <li>
                                    <h2 id= 'fospecialprice'></h2>
                                    </li>
                                  </ul>
                    </div>
                    <!-- EndProductUL -->

                    </div>
                    <div class="row">
                    </div>
                  </div>
                  <!-- EndProductInfo -->

                  <!-- StartFiedld -->
                  <div class="col-md-6 well">
                          <div class="control-group">
                          <div class="controls">
                            <input type="hidden" class="form-control" value="" id="foproduct" />
                          </div>
                          </div>

                          <div class="control-group">
                          <div class="controls">
                            <input type="hidden" class="form-control" value="" id="foprice" />
                          </div>
                          </div>

                          <div class="control-group">
                          <div class="controls">
                            <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" class="form-control" placeholder="Ваше имя" id="name" required data-validation-required-message="Обязательное поле" />
                            </div>
                          </div>
                          </div>

                          <div class="control-group">
                          <div class="controls">
                            <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                            <input type="email" class="form-control" placeholder="Email" id="emailcustomer"  />
                            </div>
                          </div>
                          </div>

                          <div class="control-group">
                          <div class="controls">
                            <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                            <input type="phone" class="form-control" placeholder="Ваш телефон" id="phone" mask="+7 (999) 999-9999" required data-validation-required-message="Обязательное поле" />
                            </div>
                          </div>
                          </div>

                          <div class="control-group">
                          <div class="controls">
                            <textarea rows="10" cols="100" class="form-control" placeholder="Ваше вопрос" id="message"  maxlength="999" style="resize:none"></textarea>
                          </div>
                          </div>
                  </div>
                  <!-- EndField -->

                  </div>
                </div>
                </div>
              </div>
                          <!-- StartModalFooter -->
                          <div class="modal-footer">
                          <div id="success"> </div>
                            <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $entry_fo_close; ?></button> -->
                            <button type="submit" class="btn btn-primary pull-right">Отправить</button>
                          </div>
                          <!-- EndModalFooter -->
              </form>
              <!-- FormEnd -->

              </div>
              <!-- EndModalBody -->

                </div>
                <!-- EndModalContent -->

                </div>
              </div>
              <!-- Modal -->
